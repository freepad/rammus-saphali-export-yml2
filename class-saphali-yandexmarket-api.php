<?php

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}


/**
 * 
 */
abstract class Saphali_Yandexmarket_API
{
	protected function get_export_status($result)
	{
		if (empty($result['export_start_time'])) {
			return 'never_running';
		}
		else {
			if (!empty($result['exception'])) {
				return 'error';
			}
			
			if (!empty($result['stopped_message'])) {
				return 'stopped';
			}
			
			if (!empty($result['export_duration'])) {
				return 'success';
			}
			elseif ($result['export_start_time'] + $this->max_execution_time + 30 > microtime(true)) {
				return 'working';
			}
		}
		return 'error';
	}
	
	protected function url_filter($url, $no_raw = false)
	{
		$url = str_replace('&#038;', '&', $url);
		if( !empty( $this->settings['fix_title'] ) ) {
			$this->settings['fix_title'] = preg_replace( '/\/\/$/' , '/', $this->settings['fix_title'] . '/');
			$url = str_replace($this->settings['fix_title'], $this->site_url, $url);
		}
		if($no_raw) return $url;
		if (!empty($url))
		{
			$_search = array (
				"'&(amp);'i",
				"'&([^;]*);'i", 
				"'&([^amp;]?)'i", 
				"'&p='i",
				);
			$_replace = array (
				"&" ,
				"&amp;\\1;",
				"&amp;",
				"&amp;p=",
			);
			if(strpos($this->site_url, 'https://') === false ) {
				$str_r = array($this->site_url, str_replace('http://', 'https://', $this->site_url) );
			} else {
				$str_r = array($this->site_url, str_replace('https://', 'http://', $this->site_url) );
			}
			$url = explode('/', str_replace($str_r, "", $url));
			$url_a = array();
			foreach($url as $v_ulr) {
				$url_a[] = urlencode($v_ulr);
			}
			$f_url = $this->site_url . implode("/", $url_a);
			$f_url = str_replace(array("%3F", '%3D', '%26'), array("?", '=', '&', '%'), $f_url);
			$url = preg_replace($_search, $_replace, htmlentities ($f_url, ENT_QUOTES , 'UTF-8'));
			return $url;
		}
		return '';
		
		
		
	}
	
	protected function text_filter(&$text, $type = '')
	{
		if (!empty($text))
		{
			$search = array (
				"/<script([^>]*)>([^<]*)<\/script>/i" ,
				"'&(amp);'i" ,
				"'&([^;]*);'i", 
				"'&([^a]+)'i", 
				"'& 'i", 
				"'&\+'i", 
				"'&-'i",
				"'&amp;laquo;'i",
				"'&amp;raquo;'i",
				"'&amp;ndash;'i",
				"'&amp;mdash;'i",
				"'&amp;&nbsp;'i",
				"'&amp;#171;'i",
				"'&amp;#187;'i",
				"'&amp;#215;'i",
				"'&amp;#8212;'i",
				"'&amp;#038;'i",
				"'&amp;apos;'i",
				"'&amp;#039;'i",
				"'&amp;#8217;'i",
				"'&amp;gt;'i",
				"'&amp;lt;'i",
				"'&amp;#x3E;'i",
				"'&amp;#x3C;'i",
			);
			$replace = array (
				"",
				"&",
				"&amp;\\1;",
				"&amp;\\1",
				"&amp; ",
				"&amp;+",
				"&amp;-",
				"&quot;",
				"&quot;",
				"-",
				"-",
				" ",
				"&quot;",
				"&quot;",
				"x",
				"-",
				"&amp;",
				"&apos;",
				"&apos;",
				"&apos;",
				"&gt;",
				"&lt;",
				"&gt;",
				"&lt;",
			);
			
			if ($type == 'description')
			{
				$search2 = array (
					"'\[&hellip;\]'i",
					"'&hellip;'i",
					"/\[([^\]]*)\]/i",
				);
				$replace2 = array (
					"...",
					"...",
					"",
				);
				return preg_replace($search, $replace, htmlentities(mb_substr(strip_tags(preg_replace($search2, $replace2, $text)), 0, 450, "UTF-8"), ENT_QUOTES , "UTF-8"));
			}
			elseif ($type == 'title') {
				return preg_replace($search, $replace, htmlentities($text, ENT_QUOTES , "UTF-8"));
			}
			else {
				return preg_replace($search, $replace, htmlentities(strip_tags($text), ENT_QUOTES , "UTF-8"));
			}
		}
		return '';
	}
	
	protected function get_all_tree_term_ids($taxonomy, $term_ids)
	{
		global $wpdb;
		
		$return = array();
		$query = "SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = '$taxonomy' AND parent = ";
		$_term_ids = $term_ids;
		while (!empty($_term_ids))
		{
			$return = array_merge($return, $_term_ids);
			$result = array();
			foreach ($_term_ids as $value) {
				$result = array_merge($result, $wpdb->get_col($query . $value));
			}
			$_term_ids = $result;
		}
		
		return $return;
	}
	
	protected function fwrite_with_retry($sock, &$data)
	{
		$bytes_to_write = strlen($data);
		$bytes_written = 0;

		while ($bytes_written < $bytes_to_write)
		{
			if ( $bytes_written == 0 ) {
				$rv = @fwrite($sock, $data);
			} else {
				$rv = @fwrite($sock, substr($data, $bytes_written));
			}

			if ( $rv === false || $rv == 0 ) {
				return $bytes_written == 0 ? false : $bytes_written;
			}

			$bytes_written += $rv;
		}
		return $bytes_written;
	}
	
	protected function get_result_as_string($result, $status)
	{
		$date = date_i18n('d.m.Y H:i', (int)$result['export_start_time'] + get_option('gmt_offset') * HOUR_IN_SECONDS);
		$next_time_msg = '';
		
		if ($result['export_type'] == 'auto')
		{
			if ($next_time = wp_next_scheduled('saphali_yandexmarket_run_auto_export_hook')) {
				$next_time = date_i18n('d.m.Y H:i', $next_time + get_option('gmt_offset') * HOUR_IN_SECONDS);
				$next_time_msg = '<br />' . sprintf(__('Ближайший запуск произойдет %s', 'saphali-yandexmarket'), $next_time);
			}
			
			if ($status == 'never_running') {
				return $next_time_msg;
			} elseif ($status == 'working') {
				return __('Выполняеться Автоматический экспорт.', 'saphali-yandexmarket');
			} elseif ($status == 'stopped') {
				return $date . ': ' . $result['stopped_message'];
			} elseif ($status == 'error') {
				$msg = !empty($result['exception']) ? $result['exception']->getMessage() : __('Автоматический экспорт был прерван из-за неизвестной ошибки (прайс может быть поврежден и его нужно создать заново).', 'saphali-yandexmarket');
				return $date . ': ' . $msg . ' ' . $next_time_msg;
			}
			$title = __('Автоматический экспорт был успешно выполнен.', 'saphali-yandexmarket');
		}
		
		else {
			if ($status == 'working') {
				return __('Выполняеться ручной экспорт.', 'saphali-yandexmarket');
			} elseif ($status == 'error') {
				return !empty($result['exception']) ? $result['exception']->getMessage() : __('Ручной экспорт был прерван из-за неизвестной ошибки (прайс может быть поврежден и его нужно создать заново).', 'saphali-yandexmarket');
			}
			$title = __('Ручной экспорт был успешно выполнен.', 'saphali-yandexmarket');
		}
		
		if ( file_exists( SYMR_PATH. 'export.yml.zip') )
			$url = SYMR_URL . 'export.yml.zip';
		else
			$url = SYMR_URL . 'export.yml';
		$look = '&nbsp;&nbsp;&nbsp;<a target="_blank" href="' . SYMR_URL . 'export.yml">' . __('Посмотреть', 'saphali-yandexmarket') . '</a>';
		$download = '&nbsp;&nbsp;&nbsp;<a href="' . SYMR_URL . 'download-price.php' . '">' . __('Скачать', 'saphali-yandexmarket') . '</a>';
		$rows[] = __('Путь к xml-файлу:', 'saphali-yandexmarket') . ' ' . $url . $look . $download;
		$rows[] = sprintf(__('Дата запуска: %s', 'saphali-yandexmarket'), $date);
		$rows[] = sprintf(__('Продолжительность: %s c (максимально допустимая - %s c)', 'saphali-yandexmarket'),	number_format($result['export_duration'], 6, '.', ' '), $result['max_execution_time']);
		$rows[] = sprintf(__('Экспортировано категорий: %s', 'saphali-yandexmarket'), number_format($result['total_terms'], 0, '.', ' '));
		$rows[] = sprintf(__('Экспортировано товаров: %s', 'saphali-yandexmarket'), number_format($result['total_products'], 0, '.', ' '));
		$rows[] = sprintf(__('Расход опреативной памяти: %s M (максимально допустимый размер - %s)', 'saphali-yandexmarket'), number_format($result['memory_peak_usage']/1048576, 0, '.', ' '), $result['memory_limit']);
		
		return '<h3>' . $title . '</h3>' . implode('<br />', $rows) . $next_time_msg;
	}
	
	protected function send_errors_report($result)/*****  доработать отправку на эмейл**********  **********  **********  ***********  *********/
	{
		$date = date_i18n('d.m.Y H:i', (int)$result['export_start_time'] + get_option('gmt_offset') * HOUR_IN_SECONDS);
		$msg = !empty($result['exception']) ? $result['exception']->getMessage() : __('Автоматический экспорт был прерван из-за неизвестной ошибки (прайс может быть поврежден и его нужно создать заново).', 'saphali-yandexmarket');
		$date . ': ' . $msg;
		add_filter( 'wp_mail_content_type', array( $this, 'get_content_type' ) );
		$headers = array();
		//$settings = get_option('saphali_yandexmarket_settings');
		$email_admin = empty( $settings['auto_export_errors_mail'] ) ? false : $settings['auto_export_errors_mail'];
		if($email_admin)
		$send_result = wp_mail(
					$email_admin,
					htmlspecialchars($date .' - '. 'экспорт в YML'),
					$msg, $headers
		);
		remove_filter( 'wp_mail_content_type', array( $this, 'get_content_type' ) );
	}
	function get_content_type() {
		return 'text/html';
	}
	/**
	 * $this->get_currency_rate_shipping('USD', 'UAH')
	 */
	function get_currency_rate_shipping($_from, $_to)
	{
		$url = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22'.$_from.$_to.'%22%29&format=json&env=store://datatables.org/alltableswithkeys&callback=';
		$response = wp_remote_get( $url, array(
				'timeout' => 45,
				'httpversion' => '1.1',
				'blocking' => true,
				'headers' => array(),
				'body' => null,
				'cookies' => array(),
				'ssl_verify' => false
			));
		if( !is_object($response) && $response["response"]["code"] == 200 && $response["response"]["message"] == "OK") {
			if( strpos($response['body'], '<') !== 0 )
			$data = json_decode($response['body']);
		}
		$rate = $data->query->results->rate->Rate;
		if ($rate <= 0) return false;
		return $rate;
	}
	
	protected function lock_tables()
	{
		global $wpdb;
		
		return $wpdb->query("
			LOCK TABLES
				$wpdb->term_relationships AS tr WRITE,
				$wpdb->term_relationships AS tr_product_type WRITE,
				$wpdb->term_relationships AS tr_vendor WRITE,
				
				$wpdb->term_taxonomy WRITE,
				$wpdb->term_taxonomy AS tt WRITE,
				$wpdb->term_taxonomy AS tt_product_type WRITE,
				$wpdb->term_taxonomy AS tt_vendor WRITE,
				$wpdb->term_taxonomy AS tt_model WRITE,
				
				$wpdb->terms AS t WRITE,
				$wpdb->terms AS t_product_type WRITE,
				$wpdb->terms AS t_vendor WRITE,
				$wpdb->terms AS t_model WRITE,
				$wpdb->terms AS ttr WRITE,
				
				$wpdb->posts AS p WRITE,
				$wpdb->posts AS p_thumbnail WRITE,
				$wpdb->posts AS p_parent WRITE,
				$wpdb->posts AS p_parent_thumbnail WRITE,
				
				$wpdb->postmeta WRITE,
				$wpdb->postmeta AS pm WRITE,
				$wpdb->postmeta AS pm_thumbnail WRITE,
				$wpdb->postmeta as parent_thumbnail WRITE,
				$wpdb->postmeta AS pm_parent_thumbnail WRITE,
				$wpdb->options WRITE,
				
				
				$wpdb->options AS o WRITE,
				
				{$wpdb->prefix}woocommerce_attribute_taxonomies WRITE
		");
	}
}
