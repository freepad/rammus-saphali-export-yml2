<?php 

include "../../../wp-load.php";
$file = 'export.yml';
if (file_exists( SYMR_PATH. 'export.yml.zip' ))
{
	header("Content-Type: application/force-download"); 
	header('Content-Disposition: attachment; filename="export.yml.zip"');
	
	if ($handle = fopen(SYMR_PATH . 'export.yml.zip', 'rb')) {
		while (!feof($handle)) {
			echo fread($handle, 1048576);
		}
		fclose($handle);
	}
} elseif (file_exists( SYMR_PATH. 'export.yml' ))
{
	header("Content-Type: application/force-download"); 
	header('Content-Disposition: attachment; filename="export.yml"');
	
	if ($handle = fopen(SYMR_PATH . 'export.yml', 'r')) {
		while (!feof($handle)) {
			echo fread($handle, 1048576);
		}
		fclose($handle);
	}
}