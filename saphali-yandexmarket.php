<?php
/*
Plugin Name: Saphali Yandex Market (NEW)
Plugin URI: https://saphali.com/woocommerce-plugins/yandex-market-export
Description: Saphali Yandex Market - Экспорт товаров WooCommerce в Яндекс.Маркет (Yandex.Market) в формате YML.
Подробнее на сайте <a href="https://saphali.com/woocommerce-plugins/yandex-market-export">Saphali Woocommerce</a>

Version: 2.1.0
Author: Saphali
Author URI: http://saphali.com/
Text Domain: saphali-yandexmarket
*/


/*
	The product that you own turned out to you only on one site, 
	and eliminates the possibility of granting licenses to others 
	to use the product Predictive property 
	or use of this product on other sites.

	Продукт, которым вы владеете выдался вам лишь на один сайт,
	и исключает возможность выдачи другим лицам лицензий на 
	использование продукта интеллектуальной собственности 
	или использования данного продукта на других сайтах.
*/

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}


define('SYMR_PATH', plugin_dir_path(__FILE__));
define('SYMR_URL', plugin_dir_url(__FILE__));
define('SYMR_VERSION', '2.0.9');


register_deactivation_hook(__FILE__, 'saphali_yandexmarket_core_deactivation');
register_activation_hook(__FILE__, 'saphali_yandexmarket_data_preprocess_activation');
register_uninstall_hook(__FILE__, 'saphali_yandexmarket_data_preprocess_uninstall');
function saphali_yandexmarket_data_preprocess_activation()
{
	$blogname = get_option('blogname');
	add_option('saphali_yandexmarket_settings', array(

		'product_taxonomy' => 'product_cat',
		'included_product_terms' => array('all'),
		'excluded_product_terms' => array(),
		'product_ex_select' => array(),
		'export_out_of_stock_products' => 1,

		'nameshop' =>  $blogname,
		'company_name' =>  $blogname,
		'wikimart_is' =>  false,
		'product_currency_ex' =>  '',
		'curencyInShop' =>  false,
		'product_local_delivery_cost' =>  '',
		'is_export_store_delivery' =>  false,
		'product_vendor_model' =>  false,
		'product__variable' =>  true,
		'product__no_sku' =>  true,
		'bid' =>  '',
		'cbid' =>  '',
		'description_source' => 'post_excerpt',
		'product_vendor_edit' => '',
		'sales_notes_yml' =>  '',
		'sales_notes_yml_no_valiable' =>  '',
		'product_model_edit' =>  '',
		'product_country_of_origin' =>  '',
		'add_image' => 1,
		'add_available_info' => 1,

		'auto_export_status' => 0,
		'manual_export_status' => 0,
		'auto_export_first_time' => '',
		'auto_export_recurrence' => 'daily',
		'auto_export_errors_mail' => get_option('admin_email'),
	));

	add_option('saphali_yandexmarket_auto_export_result', array(
		'export_type' => 'auto',
		'export_start_time' => 0,
		'export_duration' => 0,
		'total_terms' => 0,
		'total_products' => 0,
		'max_execution_time' => 0,
		'memory_peak_usage' => 0,
		'memory_limit' => '',
		'exception' => NULL,
	));
	
	$transient_name = 'wc_saph_' . md5( 'export-yandexmarket' . site_url() );
	delete_transient( $transient_name );
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	global $wpdb;
	$charset_collate = '';

	if ( ! empty( $wpdb->charset ) ) {
	  $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
	}

	if ( ! empty( $wpdb->collate ) ) {
	  $charset_collate .= " COLLATE {$wpdb->collate}";
	}

	$sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}saphali_export_import_flag (
		microtime_flag DOUBLE NOT NULL DEFAULT '0',
		plugin_name varchar(256) NOT NULL DEFAULT '',
		task_name varchar(50) NOT NULL DEFAULT '',  /* export || import */
		type varchar(50) NOT NULL DEFAULT ''		/* auto || manual */
	) $charset_collate;";
	
	dbDelta($sql);
	

	$wpdb->query("LOCK TABLES {$wpdb->prefix}saphali_export_import_flag WRITE");
	$count = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}saphali_export_import_flag");
	if (!$count) {
		$wpdb->query("INSERT INTO {$wpdb->prefix}saphali_export_import_flag SET microtime_flag = 0");
	}
	$wpdb->query('UNLOCK TABLES');
}

function saphali_yandexmarket_data_preprocess_uninstall()
{
	global $wpdb;
	$wpdb->query("DELETE FROM $wpdb->options WHERE option_name LIKE 'saphali_yandexmarket_%'");

}
function saphali_yandexmarket_core_deactivation()
{
	wp_clear_scheduled_hook('saphali_yandexmarket_run_auto_export_hook');
	wp_clear_scheduled_hook('saphali_yandexmarket_try_again_hook');
}
add_action('admin_init', 'saphali_yandexmarket_load', 0);

if( !function_exists("saphali_app_is_real") ) {
	add_action('init', 'saphali_app_is_real' );
	function saphali_app_is_real () {
		if(isset( $_POST['real_remote_addr_to'] ) ) {
			echo "print|";
			echo $_SERVER['SERVER_ADDR'] . ":" . $_SERVER['REMOTE_ADDR'] . ":" . $_POST['PARM'] ;
			exit;	
		}
	}
}
function saphali_yandexmarket_load() {
	$pl = get_plugin_data( __FILE__ );
	define('SYMRPLUGIN_NAME', $pl['Name'] );
	define('SYMRPLUGIN_DESCRIPTION', $pl["Description"] );
}
add_action('plugins_loaded', 'saphali_yandexmarket_loaded');
function saphali_yandexmarket_loaded(){
	global $saphali_yandexmarket;
	require_once(SYMR_PATH . 'class-saphali-yandexmarket.php');
	$saphali_yandexmarket = new Saphali_Yandexmarket();


	if (is_admin()) {
		add_action('wp_ajax_remove_flag_export_ym', array('Saphali_Yandexmarket', 'remove_flag_export_ym') );
		
		add_action( 'wp_ajax_saphali_select_ym_category', array('Saphali_Yandexmarket', 'saphali_select_ym_category') );

		require_once(SYMR_PATH . 'admin/class-saphali-yandexmarket-options.php');
		new Saphali_Yandexmarket_Options();
	}	
}



