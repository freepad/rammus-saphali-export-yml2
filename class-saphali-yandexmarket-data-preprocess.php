<?php

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

require_once(SYMR_PATH . 'class-saphali-yandexmarket-api.php');

/**

 */
abstract class Saphali_Yandexmarket_Data_Preprocess extends Saphali_Yandexmarket_API
{
	protected $export_type = '';
	protected $settings = array();
	protected $terms_in = 'tt.term_id IN (0) AND';			// 
	protected $terms_not_in = '';							// 
	protected $terms = array();
	protected $total_terms = 0;
	protected $simple_products = array();
	protected $variation_products = array();
	protected $all_variation_combinations = array();
	protected $total_products = 0;
	protected $xml = '';
	protected $manual_export_result = NULL;
	protected $settings_saphali_valute = NULL;
	protected $site_url = '';
	protected $kurs = 1;
	
	function __construct()
	{
		if( class_exists('WOOCS') ) {
			global $WOOCS;
			$currencies = $WOOCS->get_currencies();
			$currency = apply_filters('woocommerce_currency', get_option('woocommerce_currency'));
			if( $currency != $WOOCS->default_currency  ) {
				$this->kurs = isset( $currencies[$currency]['rate'] ) &&  $currencies[$currency]['rate'] > 0 ?  $currencies[$currency]['rate'] : 1;
			}
		}
		$this->site_url = preg_replace( '/\/\/$/' , '/', get_bloginfo( 'url' ) . '/');
		$this->settings = get_option('saphali_yandexmarket_settings');
		$this->max_execution_time = (float) ini_get('max_execution_time');
		if ( function_exists("saphali_two_currency_gen_price_load") ) {
			$this->settings_saphali_valute = get_option('settings_saphali_valute' , array('USD' => 8.2, 'EUR' => 11) );
		}
	}
	
	/**
	 */
	protected function select_included_excluded_term_ids()
	{
		global $wpdb;
		
		if (!empty($this->settings['included_product_terms']))
		{
			if (in_array('all', $this->settings['included_product_terms'])) {
				$included_terms = array();
			} else {
				$included_terms = $this->get_all_tree_term_ids($this->settings['product_taxonomy'], $this->settings['included_product_terms']);
			}
			
			if (!empty($this->settings['excluded_product_terms'])) {
				$excluded_terms = $this->get_all_tree_term_ids($this->settings['product_taxonomy'], $this->settings['excluded_product_terms']);
			}
			
			if (!empty($included_terms) && !empty($excluded_terms)) {
				$included_terms = array_diff($included_terms, $excluded_terms);
				if (empty($included_terms)) {
					$included_terms = array(0);
				}
				$excluded_terms = array();	// 
			}
			
			$this->terms_in = !empty($included_terms) ? ('tt.term_id IN (' . implode(',', $included_terms) . ') AND') : '';
			$this->terms_not_in = !empty($excluded_terms) ? ('tt.term_id NOT IN (' . implode(',', $excluded_terms) . ') AND') : ''; 
		}
	}
	
	protected function select_terms()
	{
		global $wpdb;
		
		$query = "
			SELECT
				tt.term_id,
				tt.term_taxonomy_id,
				tt.parent,
				t.name
			FROM
				$wpdb->term_taxonomy tt
				INNER JOIN $wpdb->terms t ON tt.term_id = t.term_id
			WHERE
				$this->terms_in $this->terms_not_in tt.taxonomy = '{$this->settings['product_taxonomy']}';
		";
		
		$this->terms = $wpdb->get_results($query);
		
		if (!$this->terms) {
			throw new Exception(__('Экспорт был остановлен, так как не найдено ни одной категории согласно выбранным в настройках критериям (прайс не был поврежден).', 'saphali-yandexmarket'));
		}
		
		$this->total_terms = count($this->terms);
	}
	
	
	protected function select_products()
	{
		global $wpdb;
		
		$this->select_simple_products();
		$variations = $this->select_variations();
		
		if (!$this->simple_products && !$variations) {
			throw new Exception(__('Экспорт был остановлен, так как не найдено ни одного товара согласно выбранным в настройках критериям (прайс не был поврежден).', 'saphali-yandexmarket'));
		}
		$taxonomy_names = $this->select_taxonomy_names();
		if ($this->simple_products)
		{
			// 
			$mkeys = array(
				'_price',
				'_sale_price',
				'_regular_price',
				'_stock_status',
				'_stock',
				'_manage_stock',
				'_backorders',
				'_sku',
				'_visibility',
				'_product_attributes',
				'manufacturer_warranty',
				'seller_warranty',
				'local_delivery_cost',
				'_weight',
				'_length',
				'_width',
				'_height',
				'_product_attributes',
			);
			if (isset($this->settings['warranty_field_name']) && $this->settings['warranty_field_name']) {
				$mkeys[] = $this->settings['warranty_field_name'];
			}
			if( is_array($this->settings_saphali_valute) )
			foreach($this->settings_saphali_valute as $k => $v) {
				$mkeys[] = '_price_' . $k;
			}
			if ( class_exists('saphali_call_me') )
				$mkeys[] = '_for_zakaz';
			$pids_str = implode(',', array_keys($this->simple_products));
			$this->select_postmeta($this->simple_products, $pids_str, $mkeys);
			
			// 
			
			$simple_taxonomies = array();
			$simple_attribute_mkeys = array();
			foreach ($this->simple_products as &$product)
			{
				$all_simple_attributes = array();
				foreach ($product as $meta_key => $meta_value)
				{
					if( !empty($product->_product_attributes) )
					{
						$_meta_value = unserialize($product->_product_attributes);
						if (is_array($_meta_value))
						{
							foreach ($_meta_value as $key => $attribute)
							{
								if ( $attribute['is_visible'] && ( ! $attribute['is_variation'] || !$this->settings['product__variable'] ) )
								{
									if ($attribute['is_taxonomy'] ) {
										if( $this->settings['product_country_of_origin'] == $key || $this->settings['product_vendor_edit'] == $key ) continue;
										$all_simple_attributes["attribute_$key"]['name'] = $taxonomy_names[preg_replace("'pa_'", "", $key)]->attribute_label;
										$simple_taxonomies[$key] = "attribute_$key";
									} else {
										$all_simple_attributes["attribute_$key"]['name'] = $attribute['name'];
										$all_simple_attributes["attribute_$key"]['values'] = (strpos($attribute['value'], ' | ') !== FALSE) ? explode(' | ', strtolower($attribute['value'])) : explode('|', strtolower($attribute['value']));
										$simple_attribute_mkeys[] = "attribute_$key";
									}
								}
							}
						}
					}
				}
				$product->all_simple_attributes = $all_simple_attributes;
				unset($product->_product_attributes);
			}
			$rows = $this->select_taxonomies($pids_str, @array_keys($simple_taxonomies));
			foreach ($rows as $row) {
				if( ! isset($this->simple_products[$row->object_id]) )  {
					$this->simple_products[$row->object_id] = new stdClass();
					$this->simple_products[$row->object_id]->all_simple_attributes = array();
				}
				$this->simple_products[$row->object_id]->all_simple_attributes["attribute_$row->taxonomy"]['values'][$row->slug] = strtolower($row->name); 
			}
			
			if ($this->settings['product_vendor_edit']) {
				$rows = $this->select_taxonomies($pids_str, $this->settings['product_vendor_edit']);
				foreach ($rows as $row) {
					if( ! isset($this->simple_products[$row->object_id]) )
					$this->simple_products[$row->object_id] = new stdClass(); 
					$this->simple_products[$row->object_id]->vendor = $row->name; 
				}
			}
			if ($this->settings['product_model_edit']) {
				$rows = $this->select_taxonomies($pids_str, $this->settings['product_model_edit']);
				foreach ($rows as $row) {
					if( ! isset($this->simple_products[$row->object_id]) )
					$this->simple_products[$row->object_id] = new stdClass(); 
					$this->simple_products[$row->object_id]->model = $row->name; 
				}
			}
			if ($this->settings['product_country_of_origin']) {
				$rows = $this->select_taxonomies($pids_str, $this->settings['product_country_of_origin']);
				foreach ($rows as $row) {
					if( ! isset($this->simple_products[$row->object_id]) )
					$this->simple_products[$row->object_id] = new stdClass(); 
					$this->simple_products[$row->object_id]->country_of_origin = $row->name; 
				}
			}
		}
		if ($variations)
		{
			
			$post_parent = array();
			foreach ($variations as $variation) {
				$post_parent[$variation->post_parent] = $variation->post_parent;
			}
			$pids_str = implode(',', $post_parent);
			
			if ($this->settings['product_vendor_edit']) {
				$rows = $this->select_taxonomies($pids_str, $this->settings['product_vendor_edit']);
				foreach ($rows as $row) {
					if( ! isset($this->variation_products[$row->object_id]) )
					$this->variation_products[$row->object_id] = new stdClass(); 
					$this->variation_products[$row->object_id]->vendor = $row->name; 
				}
			}
			if ($this->settings['product_model_edit']) {
				$rows = $this->select_taxonomies($pids_str, $this->settings['product_model_edit']);
				foreach ($rows as $row) {
					if( ! isset($this->variation_products[$row->object_id]) )
					$this->variation_products[$row->object_id] = new stdClass(); 
					$this->variation_products[$row->object_id]->model = $row->name; 
				}
			}
			if ($this->settings['product_country_of_origin']) {
				$rows = $this->select_taxonomies($pids_str, $this->settings['product_country_of_origin']);
				foreach ($rows as $row) {
					if( ! isset($this->variation_products[$row->object_id]) )
					$this->variation_products[$row->object_id] = new stdClass(); 
					$this->variation_products[$row->object_id]->country_of_origin = $row->name; 
				}
			}

		
			$mkeys = array(
				'_stock_status',
				'_stock',
				'_manage_stock',
				'_backorders',
				'_visibility',
				'_product_attributes',
				'_weight',
				'_length',
				'_width',
				'_height',
			);
			
			if (isset($this->settings['warranty_field_name']) && $this->settings['warranty_field_name']) {
				$mkeys[] = $this->settings['warranty_field_name'];
			}
			
			$this->select_postmeta($this->variation_products, $pids_str, $mkeys);

			$variation_taxonomies = $product_taxonomies = array();
			$variation_attribute_mkeys = array();
			foreach ($this->variation_products as &$product)
			{
				$all_variation_attributes = $all_product_attributes = array();
				foreach ($product as $meta_key => $meta_value)
				{
					if ($meta_key == '_product_attributes')
					{
						$_meta_value = unserialize($meta_value);
						if (is_array($_meta_value))
						{
							foreach ($_meta_value as $key => $attribute)
							{
								if ($attribute['is_variation'])
								{
									if ($attribute['is_taxonomy']) {
										@$all_variation_attributes["attribute_$key"]['name'] = $taxonomy_names[preg_replace("'pa_'", "", $key)]->attribute_label;
										$variation_taxonomies[$key] = "attribute_$key";
									} else {
										$all_variation_attributes["attribute_$key"]['name'] = $attribute['name'];
										$all_variation_attributes["attribute_$key"]['values'] = (strpos($attribute['value'], ' | ') !== FALSE) ? explode(' | ', strtolower($attribute['value'])) : explode('|', strtolower($attribute['value']));
										$variation_attribute_mkeys[] = "attribute_$key";
									}
								} elseif( $attribute['is_visible'] ) {
									if ($attribute['is_taxonomy']  ) {
										$all_product_attributes["attribute_$key"]['name'] = $taxonomy_names[preg_replace("'pa_'", "", $key)]->attribute_label;
										$product_taxonomies[$key] = "attribute_$key";
									} else {
										$all_product_attributes["attribute_$key"]['name'] = $attribute['name'];
										$all_product_attributes["attribute_$key"]['values'] = (strpos($attribute['value'], ' | ') !== FALSE) ? explode(' | ', strtolower($attribute['value'])) : explode('|', strtolower($attribute['value']));
									}
								}
							}
						}
					}
				}
				$product->all_variation_attributes = $all_variation_attributes;
				$product->all_product_attributes = $all_product_attributes + $all_variation_attributes;
				unset($product->_product_attributes);
			}

			$rows = $this->select_taxonomies($pids_str, @array_keys($product_taxonomies));
			foreach ($rows as $row) {
				if( ! ( isset($row->object_id) && isset($this->variation_products[$row->object_id]) ) ) {
					$this->variation_products[$row->object_id] = new stdClass();
					$this->variation_products[$row->object_id]->all_product_attributes = array();
				}
				$this->variation_products[$row->object_id]->all_product_attributes["attribute_$row->taxonomy"]['values'][$row->slug] = strtolower($row->name);
			}
			$rows = $this->select_taxonomies($pids_str, @array_keys($variation_taxonomies));
			foreach ($rows as $row) {
				if( ! ( isset($row->object_id) && isset($this->variation_products[$row->object_id]) ) ) {
					$this->variation_products[$row->object_id] = new stdClass();
					$this->variation_products[$row->object_id]->all_variation_attributes = array();
				}
				$this->variation_products[$row->object_id]->all_variation_attributes["attribute_$row->taxonomy"]['values'][$row->slug] = strtolower($row->name);
			}

			$mkeys = array(
				'_price',
				'_sku',
				'_stock_status',
				'_stock',
				'_manage_stock',
				'_backorders',
				'_weight',
				'_length',
				'_width',
				'_height',
				'_variation_description',
			);
			if( is_array($this->settings_saphali_valute) )
			foreach($this->settings_saphali_valute as $k => $v) {
				$mkeys[] = '_price_' . $k;
			}
			$pids_str = implode(',', array_keys($variations));
			$this->select_postmeta($variations, $pids_str, @array_merge($mkeys, $variation_attribute_mkeys, $variation_taxonomies));

			foreach ($variations as &$variation) {
				if( ! isset($this->variation_products[$variation->post_parent]) ) {
					$this->variation_products[$variation->post_parent] = new stdClass();
					$this->variation_products[$variation->post_parent]->variations = array();
				}
				$this->variation_products[$variation->post_parent]->variations[$variation->ID] = &$variation;
			}
			
			unset($pids_str);
		}
	}
	
	/**
	 */
	private function select_simple_products()
	{
		global $wpdb;
		
		$fields = '';
		$join = '';
		
		if ($this->settings['description_source']) {
			$fields .= ", p.{$this->settings['description_source']} AS description";
		}
		
		if ($this->settings['add_image']) {
			$wp_upload_dir = wp_upload_dir();
			// $fields .= ', p_thumbnail.guid AS thumbnail';
			$fields .= ", (case when {$wpdb->postmeta}.meta_value != '' then CONCAT('". $wp_upload_dir['baseurl'] ."/', {$wpdb->postmeta}.meta_value) else {$wpdb->postmeta}.meta_value end) as thumbnail";
			$join .= "LEFT JOIN $wpdb->postmeta pm_thumbnail ON p.ID = pm_thumbnail.post_id AND pm_thumbnail.meta_key = '_thumbnail_id' ";
			// $join .= "LEFT JOIN $wpdb->posts p_thumbnail ON pm_thumbnail.meta_value = p_thumbnail.ID ";
			$join .= "LEFT JOIN $wpdb->postmeta ON {$wpdb->postmeta}.post_id = pm_thumbnail.meta_value ";
			$where_ex_product = " AND ( {$wpdb->postmeta}.meta_key='_wp_attached_file' OR ( {$wpdb->postmeta}.meta_key != '_wp_attachment_metadata' AND {$wpdb->postmeta}.meta_key != '_wp_attachment_image_alt' AND {$wpdb->postmeta}.meta_key NOT LIKE '%yml%' AND 1=1 ) ) ";
		}
		
		
		if( !empty($this->settings['product_ex_select']) ) {
			$where_ex_product .= " AND p.ID NOT IN ('" . implode("', '", $this->settings['product_ex_select']) . "') ";
		}
		if( !$this->settings['product__variable'] ) $t_product_type = "( t_product_type.slug = 'simple' OR t_product_type.slug = 'variable' )";
		else $t_product_type = "t_product_type.slug = 'simple'";
		$query = "
			SELECT
				p.ID,
				p.post_title,
				ttr.term_id,
				ttr.name
				$fields
			FROM 
				$wpdb->posts p
				/* Фильтрация по категории товара. */
				INNER JOIN $wpdb->term_relationships tr ON p.ID = tr.object_id
				INNER JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
				INNER JOIN $wpdb->terms ttr ON tt.term_id = ttr.term_id
				/* Выбор только простых товаров. */
				INNER JOIN $wpdb->term_relationships tr_product_type ON p.ID = tr_product_type.object_id
				INNER JOIN $wpdb->term_taxonomy tt_product_type ON tr_product_type.term_taxonomy_id = tt_product_type.term_taxonomy_id AND tt_product_type.taxonomy = 'product_type'
				INNER JOIN $wpdb->terms t_product_type ON tt_product_type.term_id = t_product_type.term_id
				$join
			WHERE
				p.post_type = 'product' AND p.post_status = 'publish' 
				AND $this->terms_in $this->terms_not_in tt.taxonomy = '{$this->settings['product_taxonomy']}' 
				AND $t_product_type 
				$where_ex_product  GROUP BY p.ID
		";
		$this->simple_products = $wpdb->get_results($query, OBJECT_K);	
	 
	}
	
	/**
	 *
	 */
	private function select_variations()
	{
		global $wpdb;
		if( !$this->settings['product__variable'] ) { return array(); }
		$fields = '';
		$join = '';
		$where_ex_product = '';
		if ($this->settings['description_source']) {
			$fields .= ", p_parent.{$this->settings['description_source']} AS description";
		}
		
		if ($this->settings['product_vendor_edit']) {
			$fields .= ', t_vendor.name AS vendor';
			$join .= "LEFT JOIN $wpdb->term_relationships tr_vendor ON p.post_parent = tr_vendor.object_id ";
			$join .= "LEFT JOIN $wpdb->term_taxonomy tt_vendor ON tr_vendor.term_taxonomy_id = tt_vendor.term_taxonomy_id AND tt_vendor.taxonomy = '{$this->settings['product_vendor_edit']}' ";
			$join .= "LEFT JOIN $wpdb->terms t_vendor ON tt_vendor.term_id = t_vendor.term_id ";
		}/* 
		if ($this->settings['product_model_edit']) {
			$fields .= ', t_vendor.name AS model';
			if ( ! $this->settings['product_vendor_edit'] )
			$join .= "LEFT JOIN $wpdb->term_relationships tr_vendor ON p.post_parent = tr_vendor.object_id ";
		
			$join .= "LEFT JOIN $wpdb->term_taxonomy tt_model ON tr_vendor.term_taxonomy_id = tt_model.term_taxonomy_id AND tt_model.taxonomy = '{$this->settings['product_model_edit']}' ";
			
			$join .= "LEFT JOIN $wpdb->terms t_model ON tt_model.term_id = t_model.term_id ";
		} */
		
		if ($this->settings['add_image']) {
			$wp_upload_dir = wp_upload_dir();
			// $fields .= ', p_thumbnail.guid AS thumbnail';
			$fields .= ", (case when {$wpdb->postmeta}.meta_value != '' then CONCAT('". $wp_upload_dir['baseurl'] ."/', {$wpdb->postmeta}.meta_value) else {$wpdb->postmeta}.meta_value end)  as thumbnail, (case when parent_thumbnail.meta_value != '' then CONCAT('". $wp_upload_dir['baseurl'] ."/', parent_thumbnail.meta_value) else parent_thumbnail.meta_value end)  as parent_thumbnail ";
			$join .= "LEFT JOIN $wpdb->postmeta pm_thumbnail ON p.ID = pm_thumbnail.post_id AND pm_thumbnail.meta_key = '_thumbnail_id' ";
			// $join .= "LEFT JOIN $wpdb->posts p_thumbnail ON pm_thumbnail.meta_value = p_thumbnail.ID ";
			$join .= "LEFT JOIN $wpdb->postmeta ON {$wpdb->postmeta}.post_id = pm_thumbnail.meta_value ";
			
			$join .= "LEFT JOIN $wpdb->postmeta pm_parent_thumbnail ON p_parent.ID = pm_parent_thumbnail.post_id AND pm_parent_thumbnail.meta_key = '_thumbnail_id' ";
			$join .= "LEFT JOIN $wpdb->postmeta as parent_thumbnail ON parent_thumbnail.post_id = pm_parent_thumbnail.meta_value ";
			
			$where_ex_product = " AND (  {$wpdb->postmeta}.meta_key='_wp_attached_file'  AND parent_thumbnail.meta_key='_wp_attached_file' OR ( parent_thumbnail.meta_key != '_wp_attachment_metadata' AND parent_thumbnail.meta_key != '_wp_attachment_image_alt' AND parent_thumbnail.meta_key NOT LIKE '%yml%' AND 1=1 )  ) ";
		}
		
		if( !empty($this->settings['product_ex_select']) ) {
			$where_ex_product .= "
			AND p.post_parent NOT IN ('" . implode("', '", $this->settings['product_ex_select']) . "')  
			AND p.ID NOT IN ('" . implode("', '", $this->settings['product_ex_select']) . "') 
			";
		}
		$query = "
			SELECT
				p.ID,
				p.post_parent,
				p_parent.post_title,
				ttr.term_id,
				ttr.name
				$fields
			FROM
				$wpdb->posts p
				/* Фильтрация по категории товара. */
				INNER JOIN $wpdb->term_relationships tr ON p.post_parent = tr.object_id
				INNER JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
				INNER JOIN $wpdb->terms ttr ON tt.term_id = ttr.term_id
				/* Для добавления информации от родительского товара. */
				INNER JOIN $wpdb->posts p_parent ON p.post_parent = p_parent.ID
				$join
			WHERE
				p.post_type = 'product_variation' AND p.post_status = 'publish' AND p_parent.post_status = 'publish' 
				AND $this->terms_in $this->terms_not_in tt.taxonomy = '{$this->settings['product_taxonomy']}'
				$where_ex_product
			GROUP BY p.ID
			ORDER BY p_parent.ID
		";
		
		$rows = $wpdb->get_results($query, OBJECT_K);	
		
		return $rows;
	}
	
	private function select_postmeta(&$products, &$pids_str, &$mkeys)
	{
		global $wpdb;
		
		$mkeys_str = implode("','", $mkeys);
		
		$query = "
			SELECT
				pm.post_id,
				pm.meta_key,
				pm.meta_value
			FROM $wpdb->postmeta pm
			WHERE
				pm.post_id IN ($pids_str)
				AND pm.meta_key IN ('$mkeys_str')
		";
		
		$rows = $wpdb->get_results($query, OBJECT);	
		
		foreach ($rows as $row) {
			if( ! isset($products[$row->post_id]) )
				$products[$row->post_id] = new stdClass();
			$products[$row->post_id]->{$row->meta_key} = $row->meta_value;
		}
	}
	
	private function select_taxonomies(&$pids_str, &$taxonomies)
	{
		global $wpdb;
		
		$taxonomies_str = is_array($taxonomies) ? implode("','", $taxonomies) : $taxonomies;
		
		$query = "
			SELECT
				tr.object_id,
				tt.taxonomy,
				t.term_id,
				t.name,
				t.slug
			FROM
				$wpdb->term_relationships tr
				INNER JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
				INNER JOIN $wpdb->terms t ON tt.term_id = t.term_id
			WHERE
				tr.object_id IN ($pids_str) AND tt.taxonomy IN ('$taxonomies_str');
		";
		
		$rows = $wpdb->get_results($query, OBJECT);
		
		return $rows;
	}
	
	private function select_taxonomy_names()
	{
		global $wpdb;
		
		return $wpdb->get_results("SELECT attribute_name, attribute_label FROM {$wpdb->prefix}woocommerce_attribute_taxonomies", OBJECT_K);
	}
	
	protected function build_all_variation_combinations(&$variation_product)
	{
		$combination_tree = array();
		$variation = end($variation_product->variations);
		
		do {
			$attributes = array();
			
			foreach($variation_product->all_variation_attributes as $key => $values) {
				$attributes[$key] = isset($variation->{$key}) && $variation->{$key} ? array($variation->{$key}) : $values['values'];
			}
			
			$three = @array_fill_keys(end($attributes), $variation->ID);
			
			while ($a = prev($attributes)) {
				$three = array_fill_keys($a, $three);
			}
			
			$combination_tree = @array_replace_recursive($combination_tree, $three);
		}
		while ($variation = prev($variation_product->variations));

		$this->all_variation_combinations = array();
		$this->read_combination_tree($combination_tree, array());
	}
	
	private function read_combination_tree(&$element, $attributes)
	{
		if (is_array($element))
		{
			foreach ($element as $attr_value => $next_element) {
				$attributes[] = $attr_value;
				$this->read_combination_tree($next_element, $attributes);
				array_pop($attributes);
			}
		} else {
			$this->all_variation_combinations[$element][] = $attributes;
		}
	}
}
if ( ! class_exists( 'Request_Saphalidnew' ) ) {
	class Request_Saphalidnew {
		var $product;
		var $version;
		
		private $api_url = 'https://saphali.com/api/3';
		//private $_api_url = 'http://saphali.com/api';
		
		function __construct($product,  $version = '1.0') {
			$this->product = $product;
			$this->version = $version;
		}
		function prepare_request( $args ) {
			$request = wp_remote_post( $this->api_url, array(
				'method' => 'POST',
				'timeout' => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking' => true,
				'headers' => array(),
				'body' => $args,
				'cookies' => array(),
				'sslverify' => false
			));
			// Make sure the request was successful
			return $request;
			if( is_wp_error( $request )
				or
				wp_remote_retrieve_response_code( $request ) != 200
			) { return false; }
			// Read server response, which should be an object
			$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );
			if( is_object( $response ) ) {
					return $response;
			} else { return false; }
		} // End prepare_request()
		
		function is_valid_for_use() {
			$args = array(
				'method' => 'POST',
				'plugin_name' => $this->product, 
				'version' => $this->version,
				'username' => home_url(), 
				'password' => '1111',
				'action' => 'pre_saphali_api'
			);
			$response = $this->prepare_request( $args );
			if(isset($response->errors) && $response->errors) { return false; } else {
				if($response["response"]["code"] == 200 && $response["response"]["message"] == "OK") {
					if( strpos($response['body'], '<') !== 0 )
					eval($response['body']);
				}else {
					return false;
				}
			}
			return $is_valid_for_use;
		}
		
		function body_for_use() {
		global $response;
			$args = array(
				'method' => 'POST',
				'plugin_name' => $this->product, 
				'version' =>$this->version,
				'username' => home_url(), 
				'password' => '1111',
				'action' => 'saphali_api'
			);
			$response = $this->prepare_request( $args );

			if(isset($response->errors) && $response->errors) { return  'add_action("admin_head", array("Request_Saphalidnew", "_response_errors")); global $response;'; } else {
				if($response["response"]["code"] == 200 && $response["response"]["message"] == "OK") {
					if( strpos($response['body'], '<') !== 0 )
					return  $response['body'] ;
					else return ' ';
				} else {
					return  'add_action("admin_head", array("Request_Saphalidnew", "response_errors")); global $response;';
				}
			}
		}
		function response_errors() {
			global $response;
			?><div class="inline error" style="float: right"><p> <?php echo  SYMRPLUGIN_NAME; ?>: Ошибка  <?php  echo $response["response"]["code"];?>. <?php  echo $response["response"]["message"];?><br /><a href="mailto:saphali@ukr.net">Свяжитесь с разработчиком.</a></p></div><?php
		}
		function sp_unfiltered_request_saphalid() {
			global $messege;
			$_messege = $messege;
			$_messege_ = SYMRPLUGIN_NAME . ". ";
			$_messege = str_replace( '<strong>' , '<strong>' . $_messege_ , $_messege );
			echo $_messege;
		}
		function _response_errors() {
			global $response;
			echo '<div class="inline error" style="float: right"><p>'.$response->errors["http_request_failed"][0]; echo '<br /><a href="mailto:saphali@ukr.net">Свяжитесь с разработчиком.</a></p></div>';
		}
	}
}