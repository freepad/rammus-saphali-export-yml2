<?php

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

require_once(SYMR_PATH . 'class-saphali-yandexmarket-data-preprocess.php');

/**
 * 
 */
abstract class Saphali_Yandexmarket_Core extends Saphali_Yandexmarket_Data_Preprocess
{
	abstract protected function add_price_header_xml();
	abstract protected function add_term_xml();
	abstract protected function add_separator_xml();
	abstract protected function add_product_xml();
	abstract protected function add_price_footer_xml();
	public $return = '';
	var $archive_dir;
	var $src_dir;
	
	function __construct()
	{
		parent::__construct();
	   
		// 
		add_action('saphali_yandexmarket_run_auto_export_hook', array($this, 'run_export'));
		add_action('saphali_yandexmarket_try_again_hook', array($this, 'run_export'));
		add_action('woocommerce_product_set_stock', array($this,'new_wc_yml_cron'), 10, 1);
	}
	
	public function new_wc_yml_cron($_obj) {
		if( !$_obj->is_in_stock() ) {
			date_default_timezone_set(get_option('timezone_string'));
			$auto_export_first_time = date("H") . ':' . ( (int) date("i") + 1 );
			date_default_timezone_set('UTC');
			$settings['auto_export_recurrence'] = isset($this->settings['auto_export_recurrence']) ? $this->settings['auto_export_recurrence'] : 'twicedaily';
			$this->update_auto_export_status(true, $auto_export_first_time, $settings['auto_export_recurrence']);
		}
	}
	
	public function update_auto_export_status($activate, $auto_export_first_time, $auto_export_recurrence)
	{
		wp_clear_scheduled_hook('saphali_yandexmarket_run_auto_export_hook');
		
		update_option('saphali_yandexmarket_auto_export_result', array(
			'export_type' => 'auto',
			'export_start_time' => 0,
		));
		
		if ($activate)
		{
			// WP cron uses UTC/GMT time, not local time.
			// Therefore we set local timezone in order to mktime() will accept start-up time as local and will return as GTM.
			// For more details see http://php.net/manual/ru/function.gmmktime.php first comment.
			date_default_timezone_set(get_option('timezone_string'));
			// Get start-up time as local time.
			$t = explode(':', $auto_export_first_time);
			// mktime() return GTM time.
			$gtm_startup_time = mktime($t[0], $t[1]);
			
			if (($diffrent = time() - $gtm_startup_time) > 0)
			{
				$offset = array(
					'hourly' => 60*60,
					'twicedaily' => 12*60*60,
					'daily' => 24*60*60,
				);
				$periods = ceil($diffrent / $offset[$auto_export_recurrence]);
				$gtm_startup_time += $periods * $offset[$auto_export_recurrence];
			}

			// Set back to UTC.
			date_default_timezone_set('UTC');
			
			wp_schedule_event($gtm_startup_time, $auto_export_recurrence, 'saphali_yandexmarket_run_auto_export_hook');
		}
	}
	
	public function run_export($export_type = 'auto')
	{
		try {
			$this->export_preprocess($export_type);
			
			$Request_Saphali = new Request_Saphalidnew( 'export-yandexmarket',SYMR_VERSION );
			$transient_name = 'wc_saph_' . md5( 'export-yandexmarket' . site_url() );
			if ( false === ( $unfiltered_request_saphalid = get_transient( $transient_name ) ) ) {
				// Get all visible posts, regardless of filters
				$unfiltered_request_saphalid = $Request_Saphali->body_for_use();
				
				if( $unfiltered_request_saphalid != 'add_action("admin_head", array("Request_Saphalidnew", "_response_errors")); global $response;' && !empty($unfiltered_request_saphalid) && $Request_Saphali->is_valid_for_use() ) {
					set_transient( $transient_name, $unfiltered_request_saphalid , 60*60*24*30 );			
				}
			}
			global $messege;
			if( strpos( $unfiltered_request_saphalid, '$echo(' ) !== false || strpos( $unfiltered_request_saphalid, 'response_errors' ) !== false ) {
				eval( str_replace(array( "\$echo('" ), array( SYMR_AVALONE . SYMR_AVALTW ),  $unfiltered_request_saphalid ) );
			}
			if(isset($messege) && isset($is_valid_for_use) && !$is_valid_for_use ) {
				add_action("admin_head", array( 'Request_Saphalidnew', "sp_unfiltered_request_saphalid") );
			}
			
			unset($this->terms_in);
			unset($this->terms_not_in);
			
			$this->add_price_header_xml();
			$this->add_term_xml();
			
			unset($this->terms);
			
			$this->add_separator_xml();
			$this->add_product_xml();
			
			unset($this->simple_products);
			unset($this->variation_products);
			
			$this->add_price_footer_xml();

			$this->write_xml_in_file();
			
			unset($this->xml);
			
			$this->export_finalizing();
		}
		catch (Exception $e) {
			$this->export_finalizing($e);
		}
	}
	
	public function get_result($export_type = 'auto')
	{
		$this->settings = get_option('saphali_yandexmarket_settings');
		
		if ($export_type == 'auto' && !$this->settings['auto_export_status']) {
			return '';
		}
		if(empty($this->manual_export_result)) $this->manual_export_result = get_option('saphali_yandexmarket_manual_export_result');
		$result = ($export_type == 'auto') ? get_option('saphali_yandexmarket_auto_export_result') : $this->manual_export_result;
		$status = $this->get_export_status($result);
		
		return array(
			'status' => $status,
			'msg' => $this->get_result_as_string($result, $status),
		);
	}
	
	private function export_preprocess($export_type)
	{
		global $timestart, $wpdb;
		
		$this->export_type = $export_type;
		
		// 
		$result = $wpdb->query("
			UPDATE {$wpdb->prefix}saphali_export_import_flag
			SET microtime_flag = $timestart,
				plugin_name = 'saphali-yandexmarket',
				task_name = 'export',
				type = '$this->export_type'
			/* другой процесс не выполняется или он завершился аварийно и не смог снять флаг. */
			WHERE microtime_flag = 0 OR microtime_flag > 0 AND microtime_flag + $this->max_execution_time + 30 < " . microtime(true) . " 
		");

		
		if (!$result)
		{
			$flag = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}saphali_export_import_flag");

			if ($this->export_type == 'auto') {
				if ($flag->plugin_name != 'saphali-yandexmarket') {
					wp_schedule_single_event(time() + $this->max_execution_time + 30, 'saphali_yandexmarket_try_again_hook');
				}
				elseif ($flag->type == 'manual') {
					
					update_option('saphali_yandexmarket_auto_export_result', array(
						'export_type' => 'auto',
						'export_start_time' => $timestart,
						'export_duration' => 0,
						'stopped_message' => __('Автоматический экспорт не стартовал, потому что в момент, когда пришло время его запускать, выполнялся ручной экспорт (прайс не был поврежден).', 'saphali-yandexmarket'),
					));
				}
				exit;
			} else {
				throw new Exception(sprintf(
					__('Экспорт не стартовал, так как в данный момент выполняется другой %s %s от плагина %s. Подождите %s сек. и запустите его снова (прайс не был поврежден).', 'saphali-yandexmarket'),
					$flag->type,
					$flag->task_name,
					$flag->plugin_name,
					(int) ($flag->microtime_flag + $this->max_execution_time + 30 - microtime(true))
				));
			}
		}
		
		// 
		if ($this->export_type == 'auto')
		{
			update_option('saphali_yandexmarket_auto_export_result', array(
				'export_type' => 'auto',
				'export_start_time' => $timestart,
			));
		} else {
			update_option('saphali_yandexmarket_manual_export_result', array(
				'export_type' => $this->export_type,
				'export_start_time' => $timestart,
			));
		}
		
		// 
		if ($this->lock_tables() === FALSE) {
			throw new Exception(__('Экспорт был прерван из-за того, что пользователь БД либо не имеет прав на команду LOCK TABLES, либо не удалось заблокировать важные для экспорта таблицы БД (прайс не был поврежден).', 'saphali-yandexmarket'));
		}
	}
	public function zip_create($dir, $file) {
		$this->archive_dir = $dir;
		$this->src_dir = $dir;
		try {
			if (file_exists($this->src_dir . $file)) {
				$_zip = new ZipArchive();
				$fileName = $this->archive_dir . $file . '.zip';
				if ($_zip->open($fileName, ZIPARCHIVE::CREATE) !== true) {
					throw new Exception(__('Во время создание архива возникли ошибки.' ));
				}
				$_zip->addFile($this->src_dir . $file , $file);
					/* 
					  $dirHandle = opendir($this->src_dir);
					  while (false !== ($file = readdir($dirHandle))) {
						$_zip->addFile($this->src_dir.$file, $file);
					} */
				$_zip->close();
			}
		} catch (Exception $e) {
			$this->return = $e->getMessage();
		}
	}
	private function write_xml_in_file()
	{
		if (!$handle = @fopen(SYMR_PATH . 'export.yml', 'w')) {
			throw new Exception(__('Экспорт был прерван из-за ошибки открытия export.yml (прайс может быть поврежден и его нужно создать заново).', 'saphali-yandexmarket'));
		}
		
		$bytes = $this->fwrite_with_retry($handle, $this->xml);
		
		if (!$bytes || $bytes != strlen($this->xml)) {
			throw new Exception(__('Экспорт был прерван из-за ошибки записи в export.yml (прайс может быть поврежден и его нужно создать заново).', 'saphali-yandexmarket'));
		} else {
			$zip = $this->zip_create( SYMR_PATH , 'export.yml' );
			if($this->return) {
				if($handle = @fopen(SYMR_PATH . 'error_create_zip_file_snippet.log', 'w')) {
					fwrite($handle, date("Y.m.d H:i:s  -  ") . $zip->return);
					fclose($handle);
				}
			}
		} 
		
		if (!@fclose($handle)) {
			throw new Exception(__('Экспорт был прерван из-за ошибки закрытия export.yml (прайс может быть поврежден и его нужно создать заново).', 'saphali-yandexmarket'));
		}
	}
	
	private function export_finalizing(Exception $exception = NULL)
	{
		global $timestart, $wpdb;
		
		
		$wpdb->query('UNLOCK TABLES');
		
		$result = array(
			'export_type' => $this->export_type,
			'export_start_time' => $timestart,
			'export_duration' => microtime(true) - $timestart,
			'total_terms' => $this->total_terms,
			'total_products' => $this->total_products,
			'max_execution_time' => $this->max_execution_time,
			'memory_peak_usage' => memory_get_peak_usage(TRUE),
			'memory_limit' => WP_MEMORY_LIMIT,
			'exception' => $exception,
		);
			
		if ($this->export_type == 'auto') {
			update_option('saphali_yandexmarket_auto_export_result', $result);
			if ($exception) {
				$this->send_errors_report($result);
			}
		} else {
			$this->manual_export_result = $result;
			update_option('saphali_yandexmarket_manual_export_result', $result);
		}
		
		
		if (!$exception) {
			$wpdb->query("UPDATE {$wpdb->prefix}saphali_export_import_flag SET microtime_flag = 0, plugin_name = '', task_name = '', type = ''");
		}
	}
}
