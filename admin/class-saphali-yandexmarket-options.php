<?php

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Saphali_Yandexmarket_Options
{
	private $messages = array();
	private $warnings = array();
	private $errors = array();
	private $tabs = array();
	private $export_is_possible = TRUE;

	function __construct()
	{
		global $saphali_yandexmarket;
		
		add_action('admin_menu', array( $this, 'admin_menu'));
		if( !class_exists('CSVLoaderForWoocommerce_s') ) {
		add_action('woocommerce_process_product_meta', array($this,'woocommerce_process_product_meta_s_opt_price'), 10, 2);
		add_action('woocommerce_product_options_sku', array($this,'woocommerce_product_options_pricing_s_opt_price'),10);
		}
		
		if (!empty($_POST['create_file']))
		{
			$saphali_yandexmarket->run_export('manual');
			if ($result = $saphali_yandexmarket->get_result('manual'))
			{
				if ($result['status'] == 'error') {
					$this->errors[] = $result['msg'];
				}
				else {
					$this->messages[] = $result['msg'];
				}
			}
		}
	}
	
	function woocommerce_process_product_meta_s_opt_price($post_id, $post) {
		global $wpdb, $woocommerce, $woocommerce_errors;
		// Update post meta
		$post = array();
		if(isset($_POST['delivery_options_YM'])) {
			foreach( array('product_local_delivery_cost', 'product_local_delivery_days', 'product_local_delivery_time') as $delivery)
				foreach($_POST['delivery_options_YM'][$delivery] as $key => $v) {
					$v = trim($v);
					if(!empty( $v ) ){
						$post[$delivery][$key] = $v;
					} elseif($delivery == 'product_local_delivery_days') {
						if( isset( $post['product_local_delivery_cost'][$key] ) ) {
							unset( $post['product_local_delivery_cost'][$key] );
						}
					}
				}
		}
		update_post_meta( $post_id, 'manufacturer_warranty', stripslashes( $_POST['manufacturer_warranty'] ) );
		update_post_meta( $post_id, 'seller_warranty', stripslashes( $_POST['seller_warranty'] ) );
		update_post_meta( $post_id, 'local_delivery_cost',  $post );
	}
	function woocommerce_product_options_pricing_s_opt_price( )	{
		// Price
		global $thepostid, $post, $woocommerce;
		echo '<div>'; 
		$thepostid 		= empty( $thepostid ) ? $post->ID : $thepostid;
		$manufacturer_warranty 		= get_post_meta( $thepostid, 'manufacturer_warranty', true );
		$seller_warranty 		= get_post_meta( $thepostid, 'seller_warranty', true );
		$settings 		= get_post_meta( $thepostid, 'local_delivery_cost', true ); ?>
		<p class="form-field _opt_count_field "<?php echo ' style="border-bottom: 1px dashed #AAD2F2"';?>><label for="manufacturer_warranty"><?php _e('Гарантия производителя (для Яндекс.Маркета)', 'saphali-yandexmarket'); ?></label><select  class="wc_input_price"  name="manufacturer_warranty" id="manufacturer_warranty"> 
		<option value='' <?php if(empty($manufacturer_warranty)) echo "selected='selected'"; ?> ><?php _e('Выберите значение', 'saphali-yandexmarket'); ?></option>
		<option <?php if( $manufacturer_warranty == 'true' ) echo "selected='selected'"; ?> value='true'><?php _e('товар имеет гарантию', 'saphali-yandexmarket'); ?></option>
		<option <?php if( $manufacturer_warranty == 'false' ) echo "selected='selected'"; ?> value='false'><?php _e('товар не имеет гарантии', 'saphali-yandexmarket'); ?></option>
		<option <?php if( $manufacturer_warranty == 'P1Y' ) echo "selected='selected'"; ?> value='P1Y'><?php _e('Указать срок: 1 год', 'saphali-yandexmarket'); ?></option>
		<option <?php if( $manufacturer_warranty == 'P2Y' ) echo "selected='selected'"; ?> value='P2Y'><?php _e('Указать срок: 2 года', 'saphali-yandexmarket'); ?></option>
		<option <?php if( $manufacturer_warranty == 'P3Y' ) echo "selected='selected'"; ?> value='P3Y'><?php _e('Указать срок: 3 года', 'saphali-yandexmarket'); ?></option>
		<option value='manual' <?php if( !in_array($manufacturer_warranty , array('true', 'false', 'P1Y', 'P2Y', 'P3Y') ) && !empty($manufacturer_warranty) ) echo "selected='selected'"; ?> ><?php _e('Указать срок вручную по ISO 8601', 'saphali-yandexmarket'); ?></option>
		</select><img class="help_tip" data-tip="<?php _e('Формат параметра в случае указания срока должен соответствовать ISO 8601:P1Y2M10DT2H30M. В данном примере гарантийный срок: 1 год, 2 месяца, 10 дней, 2 часа и 30 минут.', 'saphali-yandexmarket'); ?>" src="<?php  echo $woocommerce->plugin_url() . '/assets/images/help.png';?>" height="16" width="16" /></p>
		<p class="form-field _opt_count_field "<?php echo ' style="border-bottom: 1px dashed #AAD2F2"';?>><label for="seller_warranty"><?php _e('Гарантия от продавца (для Яндекс.Маркета)', 'saphali-yandexmarket'); ?></label><select  class="wc_input_price"  name="seller_warranty" id="seller_warranty"> 
		<option value='' <?php if(empty($seller_warranty)) echo "selected='selected'"; ?> ><?php _e('Выберите значение', 'saphali-yandexmarket'); ?></option>
		<option <?php if( $seller_warranty == 'true' ) echo "selected='selected'"; ?> value='true'><?php _e('товар имеет гарантию', 'saphali-yandexmarket'); ?></option>
		<option <?php if( $seller_warranty == 'false' ) echo "selected='selected'"; ?> value='false'><?php _e('товар не имеет гарантии', 'saphali-yandexmarket'); ?></option>
		<option <?php if( $seller_warranty == 'P1Y' ) echo "selected='selected'"; ?> value='P1Y'><?php _e('Указать срок: 1 год', 'saphali-yandexmarket'); ?></option>
		<option <?php if( $seller_warranty == 'P2Y' ) echo "selected='selected'"; ?> value='P2Y'><?php _e('Указать срок: 2 года', 'saphali-yandexmarket'); ?></option>
		<option <?php if( $seller_warranty == 'P3Y' ) echo "selected='selected'"; ?> value='P3Y'><?php _e('Указать срок: 3 года', 'saphali-yandexmarket'); ?></option>
		<option value='manual' <?php if( !in_array($seller_warranty , array('true', 'false', 'P1Y', 'P2Y', 'P3Y') ) && !empty($seller_warranty) ) echo "selected='selected'"; ?> ><?php _e('Указать срок вручную по ISO 8601', 'saphali-yandexmarket'); ?></option>
		</select><img class="help_tip" data-tip="<?php _e('Формат параметра в случае указания срока должен соответствовать ISO 8601:P1Y2M10DT2H30M. В данном примере гарантийный срок: 1 год, 2 месяца, 10 дней, 2 часа и 30 минут.', 'saphali-yandexmarket'); ?>" src="<?php  echo $woocommerce->plugin_url() . '/assets/images/help.png';?>" height="16" width="16" /></p>
		<div class="form-field _opt_count_field "<?php echo ' style=""';?>><label for="local_delivery_cost"><?php _e('Условия доставки (для Яндекс.Маркета)', 'saphali-yandexmarket'); ?></label>
		<?php 
		$boolen = isset($settings['product_local_delivery_cost']) && is_array($settings['product_local_delivery_cost']);
		for($i=0; $boolen && $i < sizeof( $settings['product_local_delivery_cost']) || !$boolen && $i === 0; $i++ ) { ?>
			<div class="delivery-options">
			<span class="else">Условие №<?php echo $i+1; ?></span>
			Стоимость <br />
			<input id="product_local_delivery_cost<?php echo $i; ?>" type="number" name="delivery_options_YM[product_local_delivery_cost][]" min='0' step="10" value="<?php echo isset($settings['product_local_delivery_cost'][$i]) ? $settings['product_local_delivery_cost'][$i] : '' ; ?>" class="regular-text" />
			<hr />
			Срок <br />
			<input id="product_local_delivery_days<?php echo $i; ?>" type="text" name="delivery_options_YM[product_local_delivery_days][]" value="<?php echo isset($settings['product_local_delivery_days'][$i]) ? $settings['product_local_delivery_days'][$i] : ''; ?>" />
			<?php if($i === 0) { ?> 
				<br />
				<span class="description"><?php _e('Если срок доставки сильно варьируется, например составляет от двух до семи дней, в данном поле необходимо указать период с максимальным сроком, при этом весь период должен составлять не более трех дней по правилам передачи этой опции (допустимые периоды: 1-3, 2-4, 3-5, 4-6 и т.д.). Таким образом, для срока доставки 2‒7 дней необходимо ввести <strong>&quot;5-7&quot;</strong> или для конкретного срока доставки, если магазин доставляет все товары на следующий день то необходимо указать &quot;<strong>1</strong>&quot;.', 'saphali-yandexmarket'); ?></span>
			<?php } ?>
			<hr />
			<img class="help_tip" data-tip="<?php _e('Если срок доставки зависит от времени оформления заказа , то укажите время в часовом поясе магазина (в часах, от 0 до 24), например, если указать &quot;<strong>14</strong>&quot;, то если заказ оформлен до 14:00 текущего дня, то магазин доставляет все товары согласно сроку доставки указанного в поле &quot;Срок&quot;, а если заказ оформлен после 14:00, то товары будут доставлены на день позже указанного значения. Указание атрибута необязательно, по умолчанию используется значение 24 (полночь).', 'saphali-yandexmarket'); ?>" src="<?php  echo $woocommerce->plugin_url() . '/assets/images/help.png';?>" height="16" width="16" /> Время <br />
			<input id="product_local_delivery_time<?php echo $i; ?>" type="number" name="delivery_options_YM[product_local_delivery_time][]" min='0' max='24' step="1" value="<?php echo isset($settings['product_local_delivery_time'][$i]) ? $settings['product_local_delivery_time'][$i] : ''; ?>" class="regular-text" />
			<?php if($i === 0) { ?> 
				<br />
				<span class="description"><?php _e('Если срок доставки зависит от времени оформления заказа , то укажите время в часовом поясе магазина (в часах, от 0 до 24), например, если указать &quot;<strong>14</strong>&quot;, то если заказ оформлен до 14:00 текущего дня, то магазин доставляет все товары согласно сроку доставки указанного в поле &quot;Срок&quot;, а если заказ оформлен после 14:00, то товары будут доставлены на день позже указанного значения. Указание атрибута необязательно, по умолчанию используется значение 24 (полночь).', 'saphali-yandexmarket'); ?></span>
			<?php } ?>
			</div>
		<?php } ?>
		<span class="button add_delivery_option">Добавить еще</span>
		<div style="clear:both"></div>
		</div>
		<script type="text/javascript">
			function nth_child_delivery_option () {
				jQuery("div.delivery-options:nth-child(2n+2)").addClass('even');
				if( jQuery("div.delivery-options").length > 4 ) {
					jQuery("span.add_delivery_option").hide();
				}
			}
			jQuery(function($){
				$('body').on('click', "span.add_delivery_option", function(){
					if( $("div.delivery-options").length < 5 ) {
						$("div.delivery-options:last").after('<div class="delivery-options"><span class="else">Условие №' + ( $("div.delivery-options").length + 1) + '</span>Стоимость <br /><input id="product_local_delivery_cost' + $("div.delivery-options").length + '" type="number" name="delivery_options_YM[product_local_delivery_cost][]" min="0" step="10" value="" class="regular-text" /><hr />Срок <br /><input id="product_local_delivery_days' + $("div.delivery-options").length + '" type="text" name="delivery_options_YM[product_local_delivery_days][]" value="" /><hr />Время <br /><input id="product_local_delivery_time' + $("div.delivery-options").length + '" type="number" name="delivery_options_YM[product_local_delivery_time][]" min="0" max="24" step="1" value="" class="regular-text" /></div>');
						nth_child_delivery_option ();
					}
					
				});
			});
			nth_child_delivery_option ();	
			jQuery("select#seller_warranty").change(function(){
				if(jQuery(this).val() == "manual") {
					var value = (<?php if( !in_array($seller_warranty , array('true', 'false', 'P1Y', 'P2Y', 'P3Y') ) && !empty($seller_warranty) ) echo 0; else echo 1; ?> == 1 ) ? jQuery(this).children("option[selected='selected']").attr('value') : '<?php echo $seller_warranty; ?>';
					jQuery(this).after("<input id='seller_warranty' type='text' name='"+jQuery(this).attr('name')+"' value='"+value+"' placeholder='<?php _e('напр.', 'saphali-yandexmarket'); ?>, P1Y2M или P1Y2M10DT2H30M' />");
					jQuery(this).attr('name', 'edit'+ jQuery(this).attr('name'));
				} else {
					if(jQuery("input#seller_warranty").length > 0 ) {
						jQuery(this).attr('name', jQuery("input#seller_warranty").attr('name'));
						jQuery("input#seller_warranty").remove();
					}
				}
			});
			jQuery("select#manufacturer_warranty").change(function(){
				if(jQuery(this).val() == "manual") {
					var value = (<?php if( !in_array($manufacturer_warranty , array('true', 'false', 'P1Y', 'P2Y', 'P3Y') ) && !empty($manufacturer_warranty) ) echo 0; else echo 1; ?> == 1 ) ? jQuery(this).children("option[selected='selected']").attr('value') : '<?php echo $manufacturer_warranty; ?>';
					jQuery(this).after("<input id='manufacturer_warranty' type='text' name='"+jQuery(this).attr('name')+"' value='"+value+"'  placeholder='<?php _e('напр.', 'saphali-yandexmarket'); ?>, P1Y2M или P1Y2M10DT2H30M' />");
					jQuery(this).attr('name', 'edit'+ jQuery(this).attr('name'));
				} else {
					if(jQuery("input#manufacturer_warranty").length > 0 ) {
						jQuery(this).attr('name', jQuery("input#manufacturer_warranty").attr('name'));
						jQuery("input#manufacturer_warranty").remove();
					}
				}
			});
			jQuery("select#seller_warranty, select#manufacturer_warranty").trigger("change");
		</script>
<style>
div._opt_count_field > label {
    display: block;
    font-size: 16px;
    font-weight: bold;
    margin: 0 0 5px;
    width: 100%;
}
div._opt_count_field {
    padding: 10px;
}
div.form-field div.delivery-options > input {
	float:none;
}
div.delivery-options.even{background: #e1e1e1 none repeat scroll 0 0;}
.delivery-options span.else {display: block;font-weight: bold;}
div.delivery-options {border: 1px solid #bcbcbc;margin-bottom: 3px;padding: 10px;}
</style>
		<?php
		echo '</div>'; 
	}
	function admin_menu()
	{
		$this->menu_id = add_submenu_page(
			'edit.php?post_type=product',
			__('Экспорт товаров в YML-файл для Yandex.Market', 'saphali-yandexmarket'),
			__('Экспорт товаров YML', 'saphali-yandexmarket'),
			'manage_woocommerce',
			'yandexmarket-options',
			array($this,'options_page')
		);
		add_action( 'admin_print_scripts-' . $this->menu_id, array($this, 'frontend_scripts') );
	}
	function frontend_scripts ($hook_suffix) {
		if(isset($_GET['tab']) && $_GET['tab'] == 'options' ) {
			$params = array(
				'ajax_url'                => admin_url('admin-ajax.php'),
				'search_products_nonce'   => wp_create_nonce("search-products")
			);
			wp_enqueue_script( 'chosen' );
			wp_localize_script( 'yml-chosen-js', 'yml_admin', $params );
			wp_enqueue_script( 'yml-chosen-js' );
		}
		
	}
	
	function options_page()
	{
		global $saphali_yandexmarket;
		
		$tab = !empty($_GET['tab']) ? $_GET['tab'] : 'manual-export';
		
		$admin_url = admin_url('edit.php?post_type=product&page=yandexmarket-options');
		$this->tabs[] = array(
			'name' => __('Экспорт товаров вручную', 'saphali-yandexmarket'),
			'url' => $admin_url . '&tab=manual-export',
			'is_active' => $tab == 'manual-export',
		);
		$this->tabs[] = array(
			'name' => __('Автоматический экспорт', 'saphali-yandexmarket'),
			'url' => $admin_url . '&tab=auto-export',
			'is_active' => $tab == 'auto-export',
		);
		$this->tabs[] = array(
			'name' => __('Настройки', 'saphali-yandexmarket'),
			'url' => $admin_url . '&tab=options',
			'is_active' => $tab == 'options',
		);
		$this->tabs[] = array(
			'name' => __('Настройки заголовков', 'saphali-yandexmarket'),
			'url' => $admin_url . '&tab=options-product',
			'is_active' => $tab == 'options-product',
		);
		$this->tabs[] = array(
			'name' => __('Переопределить названия категорий', 'saphali-yandexmarket'),
			'url' => $admin_url . '&tab=options-cat',
			'is_active' => $tab == 'options-cat',
		);
		
		$currency = apply_filters('woocommerce_currency', get_option('woocommerce_currency'));
		
		if ($handle = fopen(SYMR_PATH. 'test.xml', 'a')) {
			fclose($handle);
			unlink(SYMR_PATH. 'test.xml');
		} else {
			$this->warnings[] = sprintf(__('Экспорт недоступен, т.к. нет возможности создать файл в каталоге (%s).', 'saphali-yandexmarket'), '<code>wp-content/plugins/saphali-yandexmarket/</code>');
			$this->export_is_possible = false;
		}
		
		switch ($tab)
		{
			case 'manual-export':
				$this->manual_export_tab();
				break;
				
			case 'auto-export':
				$this->auto_export_tab();
				break;
				
			case 'options':
				$this->options_tab();
				break;
			case 'options-product':
				$this->options_tab_product();
				break;
			case 'options-cat':
				$this->options_tab_cat();
				break;
		}
	}
	
	function manual_export_tab()
	{
		global $saphali_yandexmarket;
		
		$settings = get_option('saphali_yandexmarket_settings');
		
		if (!empty($_POST['create_file']) || !empty($_POST['jast_save_curs']))
		{
			$settings['curs_usd_to_uah'] = !empty($_POST['curs_usd_to_uah']) ? preg_replace('/,/i', '.', trim($_POST['curs_usd_to_uah'])) : '';
			$currency = apply_filters('woocommerce_currency', get_option('woocommerce_currency'));
			
			/* if ($currency == "USD" && empty($settings['curs_usd_to_uah'])) {
				$this->errors[] = __('Поле "Курс пересчета USD в UAH" обязательно для заполнения.', 'saphali-yandexmarket');
			}
			if ($settings['curs_usd_to_uah'] && !is_numeric($settings['curs_usd_to_uah'])) {
				$this->errors[] = __('Курс должен быть числом.', 'saphali-yandexmarket');
			} */
			
			if (empty($this->errors))
			{
				update_option('saphali_yandexmarket_settings', $settings);
				
				if (!empty($_POST['jast_save_curs'])) {
					$this->messages[] = __('Настройки сохранены.', 'saphali-yandexmarket');
				}
			}
		}

		
		if (file_exists(SYMR_PATH . "export.yml")) {
			$button_name = __('Заново создать xml-файл', 'saphali-yandexmarket');
		} else {
			$button_name = __('Создать xml-файл', 'saphali-yandexmarket');
		}
		
		$this->errors = array_merge($this->errors, $this->warnings);
		if ($result = $saphali_yandexmarket->get_result('manual'))
		{
			if ($result['status'] == 'error') {
				$this->errors[] = $result['msg'];
			}
			else {
				if($result["status"] != "never_running")
					if( array_search($result['msg'], $this->messages) === false )
						$this->messages[] = $result['msg'];
			}
		}
		require_once(SYMR_PATH . 'admin/templates/manual-export-options.php');
	}
	
	function auto_export_tab()
	{
		global $saphali_yandexmarket;
		
		$settings = get_option('saphali_yandexmarket_settings');
		
		if (!empty($_POST['submit']))
		{
			$settings['auto_export_status'] = !empty($_POST['auto_export_status']) ? 1 : 0;
			$settings['auto_export_first_time'] = !empty($_POST['auto_export_first_time']) ? preg_replace('/;/i', ':', trim($_POST['auto_export_first_time'])) : '';
			$settings['auto_export_recurrence'] = !empty($_POST['auto_export_recurrence']) ? $_POST['auto_export_recurrence'] : 'daily';
			$settings['auto_export_errors_mail'] = !empty($_POST['auto_export_errors_mail']) ? trim($_POST['auto_export_errors_mail']) : '';
		
			if (!$settings['auto_export_first_time']) {
				$this->errors[] = sprintf(__('Поле "%s" обязательно для заполнения.', 'saphali-yandexmarket'), __('Время запуска', 'saphali-yandexmarket'));
			}
			elseif (!preg_match("/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/i", $settings['auto_export_first_time'])) {
				$this->errors[] = __('Указано неправильное время.', 'saphali-yandexmarket');
			}
			if (!empty($settings['auto_export_errors_mail']) && !is_email($settings['auto_export_errors_mail'])) {
				$this->errors[] = __('Неверный формат E-mail адреса.', 'saphali-yandexmarket');
			}
			
			if (empty($this->errors))
			{
				update_option('saphali_yandexmarket_settings', $settings);
				$this->messages[] = __('Настройки сохранены.', 'saphali-yandexmarket');
				
				$saphali_yandexmarket->update_auto_export_status($settings['auto_export_status'], $settings['auto_export_first_time'], $settings['auto_export_recurrence']);
			}
		}
		
		$this->errors = array_merge($this->errors, $this->warnings);

		if ($result = $saphali_yandexmarket->get_result('auto'))
		{
			if ($result['status'] == 'error') {
				$this->errors[] = $result['msg'];
			}
			else {
				$this->messages[] = $result['msg'];
			}
		}
					
		require_once(SYMR_PATH . 'admin/templates/auto-export-options.php');
	}
	
	function options_tab_cat() {
		global $saphali_yandexmarket, $wpdb;
		if (!empty($_POST['submit']))
		{
			$yml_product_title_replace = get_option('yml_product_cat_replace', array());
			$_POST['title'] = empty($_POST['title']) ? array() : $_POST['title'];
			$_title = array();
			$title = $_POST['title'];
			foreach($_POST['title'] as $k => $v) {
				if(empty($v)) unset( $title[$k] );
			}
			$p_keys = array_keys ( $title );
			$p__keys = array_keys ( $_POST['title'] );
			$s_keys = array_keys ( $yml_product_title_replace );
			$_result1 = array_diff ($p_keys, $s_keys);
			$_result2 = array_diff  ($s_keys, $p__keys);
			$_result3 = array_intersect ($p_keys, $s_keys);
			$result = array_merge($_result1, $_result2, $_result3);
			
			foreach($result as $ID) {
				if(isset($title[$ID]))
					$_title[$ID] = stripcslashes ( $title[$ID] );
				else 
					$_title[$ID] = stripcslashes ( $yml_product_title_replace[$ID] );
			}
			
			update_option('yml_product_cat_replace', $_title);
		}
		require_once(SYMR_PATH . 'admin/templates/other-options-pr-cat.php');
	}
	function options_tab_product() {
		if (!empty($_POST['submit']))
		{
			$yml_product_title_replace = get_option('yml_product_title_replace', array());
			$_POST['title'] = empty($_POST['title']) ? array() : $_POST['title'];
			$_title = array();
			$title = $_POST['title'];
			foreach($_POST['title'] as $k => $v) {
				if(empty($v)) unset( $title[$k] );
			}
			$p_keys = array_keys ( $title );
			$p__keys = array_keys ( $_POST['title'] );
			$s_keys = array_keys ( $yml_product_title_replace );
			$_result1 = array_diff ($p_keys, $s_keys);
			$_result2 = array_diff  ($s_keys, $p__keys);
			$_result3 = array_intersect ($p_keys, $s_keys);
			$result = array_merge($_result1, $_result2, $_result3);
			
			foreach($result as $ID) {
				if(isset($title[$ID]))
					$_title[$ID] = stripcslashes ( $title[$ID] );
				else 
					$_title[$ID] = stripcslashes ( $yml_product_title_replace[$ID] );
			}
			
			update_option('yml_product_title_replace', $_title);
		} 
		require_once(SYMR_PATH . 'admin/templates/other-options-pr-title.php');
	}
	function options_tab()
	{
		global $saphali_yandexmarket, $wpdb;
		
		$settings = get_option('saphali_yandexmarket_settings');
		
		if (!empty($_POST['submit']))
		{
			$settings['product_vendor_model'] = !empty($_POST['product_vendor_model']) ? true : false;
			$settings['product_no_typePrefix'] = !empty($_POST['product_no_typePrefix']) ? true : false;
			$settings['product__variable'] = !empty($_POST['product__variable']) ? true : false;
			$settings['product__no_sku'] = !empty($_POST['product__no_sku']) ? true : false;
			
			$settings['bid'] = !empty($_POST['bid']) ? $_POST['bid'] : '';
			$settings['cbid'] = !empty($_POST['cbid']) ? $_POST['cbid'] : '';
			
			
			$settings['product_vendor_edit'] = !empty($_POST['product_vendor_edit']) ? $_POST['product_vendor_edit'] : '';
			$settings['product_model_edit'] = !empty($_POST['product_model_edit']) ? $_POST['product_model_edit'] : '';
			$settings['product_country_of_origin'] = !empty($_POST['product_country_of_origin']) ? $_POST['product_country_of_origin'] : '';
			
			$settings['sales_notes_yml'] = !empty($_POST['sales_notes_yml']) ? $_POST['sales_notes_yml'] : '';
			$settings['sales_notes_yml_no_valiable'] = !empty($_POST['sales_notes_yml_no_valiable']) ? $_POST['sales_notes_yml_no_valiable'] : '';
			if(sizeof($_POST['included_product_terms']) > 1 ) {
				if( ($search = array_search('all', $_POST['included_product_terms'])) !== false ) {
					unset($_POST['included_product_terms'][$search]);
				}
			}
			$settings['included_product_terms'] = !empty($_POST['included_product_terms']) ? $_POST['included_product_terms'] : array();
			
			$settings['_product_terms_in_market'] = !empty($_POST['_product_terms_in_market']) ? $_POST['_product_terms_in_market'] : array();
			$settings['product_terms_in_market'] = !empty($_POST['product_terms_in_market']) ? $_POST['product_terms_in_market'] : array();
			
			$settings['excluded_product_terms'] = !empty($_POST['excluded_product_terms']) ? $_POST['excluded_product_terms'] : array();
			$settings['product_ex_select'] = !empty($_POST['product_ex_select']) ? $_POST['product_ex_select'] : array();
			$settings['export_out_of_stock_products'] = !empty($_POST['export_out_of_stock_products']) ? false : true;
			
			$settings['export_out_of_stock_products_force'] = empty($_POST['export_out_of_stock_products_force']) ? false : true;
			
			$settings['nameshop'] = !empty($_POST['nameshop']) ? $_POST['nameshop'] : '';
			$settings['company_name'] = !empty($_POST['company_name']) ? $_POST['company_name'] : '';
			
			$settings['wikimart_is'] = !empty($_POST['wikimart_is']) ? true : false;
			$settings['curencyInShop'] = !empty($_POST['curencyInShop']) ? true : false;
			$settings['product_taxonomy'] = !empty($_POST['product_taxonomy']) ? $_POST['product_taxonomy'] : '';
			$settings['product_currency_ex'] = !empty($_POST['product_currency_ex']) ? $_POST['product_currency_ex'] : '';
			$settings['product_currency_USD'] = !empty($_POST['product_currency_USD']) ? $_POST['product_currency_USD'] : '';
			$settings['product_currency_EUR'] = !empty($_POST['product_currency_EUR']) ? $_POST['product_currency_EUR'] : '';
			$settings['fix_title'] = !empty($_POST['fix_title']) ? $_POST['fix_title'] : '';
			$settings['product_currency_UAH'] = !empty($_POST['product_currency_UAH']) ? $_POST['product_currency_UAH'] : '';
						
			$post = array();
			if(isset($_POST['product_local_delivery_cost'])) {
				foreach( array('product_local_delivery_cost', 'product_local_delivery_days', 'product_local_delivery_time') as $delivery)
					foreach($_POST[$delivery] as $key => $v) {
						$v = trim($v);
						if(!empty( $v ) ){
							$post[$delivery][$key] = $v;
						} elseif($delivery == 'product_local_delivery_days') {
							if( isset( $post['product_local_delivery_cost'][$key] ) ) {
								unset( $post['product_local_delivery_cost'][$key] );
							}
						}
					}
				if( !empty($post) )
					unset( $settings['product_local_delivery_cost'] );
			}
			$settings['product_local_delivery_cost_all'] = $post;
			
			
			$settings['is_export_store_delivery'] = !empty($_POST['is_export_store_delivery']) ? true : false;
			
			$settings['is_export_delivery'] = !empty($_POST['is_export_delivery']) ? true : false;
			$settings['is_export_delivery_echo'] = !empty($_POST['is_export_delivery_echo']) ? true : false;
			
			$settings['is_export_pickup'] = !empty($_POST['is_export_pickup']) ? true : false;
			$settings['is_export_pickup_echo'] = !empty($_POST['is_export_pickup_echo']) ? true : false;
			
			$settings['is_export_store'] = !empty($_POST['is_export_store']) ? true : false;
			$settings['is_export_store_echo'] = !empty($_POST['is_export_store_echo']) ? true : false;
			
			$settings['description_source'] = !empty($_POST['description_source']) ? $_POST['description_source'] : '';
			//$settings['vendor_taxonomy'] = !empty($_POST['vendor_taxonomy']) ? trim($_POST['vendor_taxonomy']) : '';
			//$settings['warranty_field_name'] = !empty($_POST['warranty_field_name']) ? trim($_POST['warranty_field_name']) : '';
			$settings['add_image'] = !empty($_POST['add_image']) ? 1 : 0;
			$settings['add_available_info'] = !empty($_POST['add_available_info']) ? 1 : 0;
			
			if (!$settings['nameshop']) {
				//update_option('saphali_yandexmarket_settings', $settings);
				$this->errors[] = sprintf(__('Поле "%s" обязательно для заполнения.', 'saphali-yandexmarket'), __('Короткое название магазина (name)', 'saphali-yandexmarket'));
			}
			
			if (empty($this->errors))
			{
				update_option('saphali_yandexmarket_settings', $settings);
				$this->messages[] = __('Настройки сохранены.', 'saphali-yandexmarket');
			}
		}

		$taxonomies = get_taxonomies(array('object_type' => array('product')), 'objects');
		$all_product_terms = get_terms($settings['product_taxonomy'], 'orderby=name&hide_empty=0');
		$meta_keys = $wpdb->get_col("SELECT DISTINCT meta_key FROM $wpdb->postmeta ORDER BY meta_key");
		
		$depth = array();
		foreach ($all_product_terms as $cat)
		{
			if ($cat->parent) {
				$cat_parent = !empty($depth[$cat->parent]) ? $depth[$cat->parent] : '';
				$depth[$cat->term_id] = $cat_parent . '.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			} else {
				$depth[$cat->term_id] = '';
			}
		}
		
		$this->errors = array_merge($this->errors, $this->warnings);
		require_once(SYMR_PATH . 'admin/templates/other-options.php');
	}
	function get_currency_rate_shipping($_from, $_to)
	{
		$url = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22'.$_from.$_to.'%22%29&format=json&env=store://datatables.org/alltableswithkeys&callback=';
		$response = wp_remote_get( $url, array(
				'timeout' => 45,
				'httpversion' => '1.1',
				'blocking' => true,
				'headers' => array(),
				'body' => null,
				'cookies' => array(),
				'ssl_verify' => false
			));
		if( !is_object($response) && $response["response"]["code"] == 200 && $response["response"]["message"] == "OK") {
			if( strpos($response['body'], '<') !== 0 )
			$data = json_decode($response['body']);
		}
		$rate = $data->query->results->rate->Rate;
		if ($rate <= 0) return false;
		return $rate;
	}
}

	

	


