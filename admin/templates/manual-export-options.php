<?php

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>
<div class="wrap">

	<h2 class="nav-tab-wrapper">
		<?php foreach ($this->tabs as $tab): ?>
		<a class="nav-tab<?php if ($tab['is_active']) echo ' nav-tab-active'; ?>" href="<?php echo $tab['url']; ?>"><?php echo $tab['name']; ?></a>
		<?php endforeach; ?>
	</h2>
	
	<?php if (!empty($this->errors)): ?>
	<div class="error">
		<?php foreach ($this->errors as $err): ?>
		<p><strong><?php echo $err; ?></strong></p>
		<?php endforeach; ?>
		<script type="text/javascript" >
		jQuery(document).ready(function($) {
			$('body').delegate('.remove_flag_export_ym', 'click', function(event) {
				event.preventDefault();
				var el_flag = $(this);
				var el_flag_bg = el_flag.css('background');
				el_flag.css({'background': "#ccc", cursor: 'wait'});
				var data = {
					'action': 'remove_flag_export_ym'
				};
				$.post(ajaxurl, data, function(response) {
					if(response) {
						el_flag.css({'background': el_flag_bg, cursor: 'default'});
						el_flag.text('Сброшено');
					}
				});
			});
			
		});
		</script> 
		<button class="remove_flag_export_ym button">Сбросить счетчик</button>
	</div>
	<?php endif; ?>
	
	<?php if (!empty($this->messages)): ?>
	<div class="updated">
		<?php foreach ($this->messages as $msg): ?>
		<p><strong><?php echo $msg; ?></strong></p>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
	
	<p><?php _e('Оба вида экспорта обновляют тот же самый xml-файл (в момент успешного завершения всех операций), выполняются одним махом и длятся несколько секунд. Экспорт 5000 товаров на сильных серверах длится около 8с. Для такого количества товаров нужно около 50Мб оперативной памяти. Для экспорта большего количества товаров нужно увеличить max_execution_time и memory_limit, иначе он завершится аварийно и прайс может быть поврежден.', 'saphali-yandexmarket'); ?></p>
	<p><?php _e('Если запустить экспорт любого вида в момент выполнения другого экспорта любого вида, то первый немедленно завершится и прайс поврежден не будет.', 'saphali-yandexmarket'); ?></p>
	
	<form method="post" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<p class="submit">
			<?php submit_button($button_name, 'secondary', 'create_file', false, (!$this->export_is_possible) ? array('disabled' => 'disabled') : array()); ?>
		</p>
		
	</form>
</div>
