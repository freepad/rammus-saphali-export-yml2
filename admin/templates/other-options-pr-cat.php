<div class="wrap">

	<h2 class="nav-tab-wrapper">
		<?php foreach ($this->tabs as $tab): ?>
		<a class="nav-tab<?php if ($tab['is_active']) echo ' nav-tab-active'; ?>" href="<?php echo $tab['url']; ?>"><?php echo $tab['name']; ?></a>
		<?php endforeach; ?>
	</h2>
	
	<?php if (!empty($this->errors)): ?>
	<div class="error">
		<?php foreach ($this->errors as $err): ?>
		<p><strong><?php echo $err; ?></strong></p>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>

	<?php if (!empty($this->messages)): ?>
	<div class="updated">
		<?php foreach ($this->messages as $msg): ?>
		<p><strong><?php echo $msg; ?></strong></p>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
	<form method="post" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<h3><?php _e('Настройки категории товара', 'saphali-yandexmarket'); ?></h3>
		<table class="form-table">
			<thead>
			<th scope="row"><label for="product_taxonomy"><?php _e('Оригинальное название', 'saphali-yandexmarket'); ?></label></th>
			<th scope="row" style="width: 30px;"><label for="product_taxonomy"><?php _e('ID', 'saphali-yandexmarket'); ?></label></th>
			<th scope="row"><label for="product_taxonomy"><?php _e('Отредактированное название', 'saphali-yandexmarket'); ?></label></th>
			</thead>
			<tbody>
			<?php 
$settings = get_option('saphali_yandexmarket_settings');
$all_product_terms = get_terms($settings['product_taxonomy'], 'orderby=name&hide_empty=0');
$yml_product_title_replace = get_option('yml_product_cat_replace', array());
foreach ($all_product_terms as $cat) {
	?>
				<tr>
					<td><span style="cursor: pointer;"><?php echo $cat->name; ?> </td>
					<td><?php echo $cat->term_id; ?></td>
					<td> 
						<textarea style="float:left" rel="<?php echo $c; ?>" <?php if(! empty($yml_product_title_replace[$cat->term_id])) { echo 'class="is_replace"'; $hide = true; } ?> name="title[<?php echo $cat->term_id; ?>]"><?php echo $yml_product_title_replace[$cat->term_id]; ?></textarea> 
					</td>
				</tr>
	<?php
}
?>
			</tbody>
		</table>
<?php
	if ( $count > 1 ) : ?>
<div class="clear"></div>
<nav class="woocommerce-pagination">
	<div class="navigation_set">	
			<?php
			$args = array(
			'base' 			=> str_replace( 999999999, '%#%', get_pagenum_link( 999999999 ) ),
			'format' 		=> '?paged=%#%',
			'current' 		=> $_count,
			'total' 		=> $count,
			'prev_text' 	=> '&larr;',
			'next_text' 	=> '&rarr;',
			'type'			=> 'list',
			'end_size'		=> 3,
			'mid_size'		=> 3
		);
			echo str_replace( array( '&#038;','#038;', "tab=options-product&page=yandexmarket-options&tab=options-product&paged"), array('&','&', 'tab=options-product&paged' ), paginate_links( $args ) );
			?>
	</div>
<style type="text/css">nav.woocommerce-pagination ul.page-numbers li a {background: none repeat scroll 0 0 #FFFFFF;color: black;padding: 3px;}nav.woocommerce-pagination ul li a, nav.woocommerce-pagination ul.page-numbers li a, nav.woocommerce-pagination ul li span {display: block;font-size: 1em;font-weight: normal;line-height: 1em;margin: 0;min-width: 1em;padding: 0.5em;text-decoration: none;}nav.woocommerce-pagination ul li {border-right: 1px solid #2C88B4;display: inline;float: left;margin: 0;overflow: hidden;padding: 0;}nav.woocommerce-pagination ul.page-numbers li a:focus, nav.woocommerce-pagination ul.page-numbers li span.current, nav.woocommerce-pagination ul.page-numbers li a:hover {background: none repeat scroll 0 0 #64ADDD;color: white;}nav.woocommerce-pagination ul.page-numbers li {border-right: medium none;height: auto;margin-right: 2px;}nav.woocommerce-pagination {border-top: 1px solid #469DD3;text-align: left;}.pers-list .text-list {border-bottom: 1px solid #AEAEAF;list-style: none outside none;padding: 0;}.pers-list .even {background: none repeat scroll 0 0 #F5F5F5;}.text-list li {padding: 7px !important; list-style: none outside none !important;float: none !important;}li.comment img.avatar { height: auto;float: left;padding: 0;width: 130px; margin: 0 10px 5px 0;}.reviews p {font-style: italic;}.comment-text p strong {font-family: arial,Trebuchet MS;font-style: normal;}.comment-text div.description, .comment-text div.description p {clear: none;} p.extra_filds {text-align: right; padding-right: 10px;}.pers-list li.odd {background: none repeat scroll 0 0 #fff;border-bottom:1px dashed #D2D1D1;border-top:1px dashed #D2D1D1;}.pers-list li:last-child {border-bottom: medium none !important;}
.bar
{
background-color:#5fbbde;
width:0px;
height:16px;
}
.barbox
{
float:right; 
height:16px; 
background-color:#FFFFFF; 
width:100px; 
border:solid 1px #000; 
margin-right:3px;
-webkit-border-radius:5px;-moz-border-radius:5px;
}
.count
{
float:right; margin-right:8px; 
font-family:'Georgia', Times New Roman, Times, serif; 
font-size:16px; 
font-weight:bold; 
color:#666666
}
</style>
</nav>
<div class="clear"></div>
<?php else: ?>
<style type="text/css"> textarea.is_replace {border: 1px solid green;}
.bar
{
background-color:#5fbbde;
width:0px;
height:16px;
}
.barbox
{
float:right; 
height:16px; 
background-color:#FFFFFF; 
width:100px; 
border:solid 1px #000; 
margin-right:3px;
-webkit-border-radius:5px;-moz-border-radius:5px;
}
.count
{
float:right; margin-right:8px; 
font-family:'Georgia', Times New Roman, Times, serif; 
font-size:16px; 
font-weight:bold; 
color:#666666
} </style>
		<?php endif; ?>
		<br />
		<input type="submit" name="submit" class="button-primary" value="<?php _e("Save"); ?> " /> &nbsp;&nbsp;&nbsp; <?php if($hide) echo '<input class="button but" value="Очистить поля" />'; ?>
		
	</form>
<script>
jQuery(".but").click(function() {
	jQuery("table td textarea").each(function(){
		jQuery(this).val('');
	});
});
jQuery("table td span").click(function() {
	if( jQuery(this).parent().parent().find("textarea").val() == '' ) {
		jQuery(this).parent().parent().find("textarea").val(jQuery(this).text());
		jQuery(this).parent().parent().find("textarea").addClass('is_replace');
		jQuery(this).parent().parent().find("textarea").trigger('keyup');
	}
});
jQuery("table td textarea").blur(function() {
	if( jQuery(this).val() == '' ) {
		jQuery(this).removeClass('is_replace');
	} else {
		jQuery(this).addClass('is_replace');
	}
});
jQuery(document).ready(function($)
{
	$("table td textarea").keyup(function()
	{
	var box=$(this).val();
	var rel=$(this).attr('rel');
	var main = box.length *100;
	var value= (main / 70);
	var count= box.length;
	 
	if(box.length <= 70)
	{
	$('#count' + rel).html(count);
	$('#bar' + rel).animate(
	{
	"width": value+'%',
	}, 1);
	}
	else
	{
		$('#count' + rel).html(count);
	}
	return false;
	});
	 
});
</script>
</div>