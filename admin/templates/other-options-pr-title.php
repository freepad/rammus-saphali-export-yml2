<div class="wrap">

	<h2 class="nav-tab-wrapper">
		<?php foreach ($this->tabs as $tab): ?>
		<a class="nav-tab<?php if ($tab['is_active']) echo ' nav-tab-active'; ?>" href="<?php echo $tab['url']; ?>"><?php echo $tab['name']; ?></a>
		<?php endforeach; ?>
	</h2>
	
	<?php if (!empty($this->errors)): ?>
	<div class="error">
		<?php foreach ($this->errors as $err): ?>
		<p><strong><?php echo $err; ?></strong></p>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>

	<?php if (!empty($this->messages)): ?>
	<div class="updated">
		<?php foreach ($this->messages as $msg): ?>
		<p><strong><?php echo $msg; ?></strong></p>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
	<form method="post" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>">
	<h3><?php _e('Поиск товара по заголовку и/или описанию', 'saphali-yandexmarket'); ?></h3>
	
	<input type="text" name="s" value="<?php echo $_POST['s']; ?>" />
	<input type="submit" name="search" class="button-primary" value="<?php _e("Search"); ?> " />
	</form>
	<form method="post" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<h3><?php _e('Настройки заголовков товара', 'saphali-yandexmarket'); ?></h3>
		<table class="form-table">
			<thead>
			<th scope="row"><label for="product_taxonomy"><?php _e('Оригинальный заголовок', 'saphali-yandexmarket'); ?></label></th>
			<th scope="row" style="width: 30px;"><label for="product_taxonomy"><?php _e('ID товара', 'saphali-yandexmarket'); ?></label></th>
			<th scope="row" style="width: 30px;"><label for="product_taxonomy"><?php _e('Артикул', 'saphali-yandexmarket'); ?></label></th>
			<th scope="row"><label for="product_taxonomy"><?php _e('Отредактированный заголовок', 'saphali-yandexmarket'); ?></label></th>
			</thead>
			<tbody>
			<?php 
$_count = isset( $_GET['paged'] ) ? $_GET['paged'] : 1 ;
$args = array(
	'posts_per_page' => 20,
	'post_status' => 'publish',
	'post_type' => 'product',
	'paged' => $_count
);
if(isset($_POST['s'])) {
$args['s'] = $_POST['s'];
}
$the_query = new WP_Query( $args );
$count = $the_query->max_num_pages;
$hide = false;
$yml_product_title_replace = get_option('yml_product_title_replace', array());
$c = 0;
while ( $the_query->have_posts() ) {
	$c++;
	$the_query->the_post();
	?>
				<tr>
					<td><span style="cursor: pointer;"><?php $get_the_title = get_the_title(); echo $get_the_title; ?></span> <?php echo " (<strong>" . mb_strlen($get_the_title, 'UTF-8') . "</strong> симв.)"; ?> </td>
					<td><?php echo $the_query->post->ID; ?></td>
					<td><?php echo get_post_meta( $the_query->post->ID, '_sku', true); ?></td>
<td> <textarea style="float:left" rel="<?php echo $c; ?>" <?php if(! empty($yml_product_title_replace[$the_query->post->ID])) { echo 'class="is_replace"'; $hide = true; } ?> name="title[<?php echo $the_query->post->ID; ?>]"><?php echo $yml_product_title_replace[$the_query->post->ID]; ?></textarea> 

<div style="float:left;margin-top: 35px;"><div id="barbox<?php echo $c; ?>" class="barbox"><div class="bar" style="width: <?php echo ((float)mb_strlen( (isset($yml_product_title_replace[$the_query->post->ID]) ? $yml_product_title_replace[$the_query->post->ID]: ''), 'UTF-8' ) / 70 * 100); ?>%" id="bar<?php echo $c; ?>"></div></div><div class="count" id="count<?php echo $c; ?>"><?php echo mb_strlen( (isset($yml_product_title_replace[$the_query->post->ID]) ? $yml_product_title_replace[$the_query->post->ID]: ''), 'UTF-8'); ?></div></div></td>
				</tr>
	<?php
}
wp_reset_query();
wp_reset_postdata();
?>
			</tbody>
		</table>
<?php
	if ( $count > 1 ) : ?>
<div class="clear"></div>
<nav class="woocommerce-pagination">
	<div class="navigation_set">	
			<?php
			$args = array(
			'base' 			=> str_replace( 999999999, '%#%', get_pagenum_link( 999999999 ) ),
			'format' 		=> '?paged=%#%',
			'current' 		=> $_count,
			'total' 		=> $count,
			'prev_text' 	=> '&larr;',
			'next_text' 	=> '&rarr;',
			'type'			=> 'list',
			'end_size'		=> 3,
			'mid_size'		=> 3
		);
			echo str_replace( array( '&#038;','#038;', "tab=options-product&page=yandexmarket-options&tab=options-product&paged"), array('&','&', 'tab=options-product&paged' ), paginate_links( $args ) );
			?>
	</div>
<style type="text/css">nav.woocommerce-pagination ul.page-numbers li a {background: none repeat scroll 0 0 #FFFFFF;color: black;padding: 3px;}nav.woocommerce-pagination ul li a, nav.woocommerce-pagination ul.page-numbers li a, nav.woocommerce-pagination ul li span {display: block;font-size: 1em;font-weight: normal;line-height: 1em;margin: 0;min-width: 1em;padding: 0.5em;text-decoration: none;}nav.woocommerce-pagination ul li {border-right: 1px solid #2C88B4;display: inline;float: left;margin: 0;overflow: hidden;padding: 0;}nav.woocommerce-pagination ul.page-numbers li a:focus, nav.woocommerce-pagination ul.page-numbers li span.current, nav.woocommerce-pagination ul.page-numbers li a:hover {background: none repeat scroll 0 0 #64ADDD;color: white;}nav.woocommerce-pagination ul.page-numbers li {border-right: medium none;height: auto;margin-right: 2px;}nav.woocommerce-pagination {border-top: 1px solid #469DD3;text-align: left;}.pers-list .text-list {border-bottom: 1px solid #AEAEAF;list-style: none outside none;padding: 0;}.pers-list .even {background: none repeat scroll 0 0 #F5F5F5;}.text-list li {padding: 7px !important; list-style: none outside none !important;float: none !important;}li.comment img.avatar { height: auto;float: left;padding: 0;width: 130px; margin: 0 10px 5px 0;}.reviews p {font-style: italic;}.comment-text p strong {font-family: arial,Trebuchet MS;font-style: normal;}.comment-text div.description, .comment-text div.description p {clear: none;} p.extra_filds {text-align: right; padding-right: 10px;}.pers-list li.odd {background: none repeat scroll 0 0 #fff;border-bottom:1px dashed #D2D1D1;border-top:1px dashed #D2D1D1;}.pers-list li:last-child {border-bottom: medium none !important;}
.bar
{
background-color:#5fbbde;
width:0px;
height:16px;
}
.barbox
{
float:right; 
height:16px; 
background-color:#FFFFFF; 
width:100px; 
border:solid 1px #000; 
margin-right:3px;
-webkit-border-radius:5px;-moz-border-radius:5px;
}
.count
{
float:right; margin-right:8px; 
font-family:'Georgia', Times New Roman, Times, serif; 
font-size:16px; 
font-weight:bold; 
color:#666666
}
</style>
</nav>
<div class="clear"></div>
<?php else: ?>
<style type="text/css"> textarea.is_replace {border: 1px solid green;}
.bar
{
background-color:#5fbbde;
width:0px;
height:16px;
}
.barbox
{
float:right; 
height:16px; 
background-color:#FFFFFF; 
width:100px; 
border:solid 1px #000; 
margin-right:3px;
-webkit-border-radius:5px;-moz-border-radius:5px;
}
.count
{
float:right; margin-right:8px; 
font-family:'Georgia', Times New Roman, Times, serif; 
font-size:16px; 
font-weight:bold; 
color:#666666
} </style>
		<?php endif; ?>
		<br />
		<input type="submit" name="submit" class="button-primary" value="<?php _e("Save"); ?> " /> &nbsp;&nbsp;&nbsp; <?php if($hide) echo '<input class="button but" value="Очистить поля" />'; ?>
		
	</form>
<script>
jQuery(".but").click(function() {
	jQuery("table td textarea").each(function(){
		jQuery(this).val('');
	});
});
jQuery("table td span").click(function() {
	if( jQuery(this).parent().parent().find("textarea").val() == '' ) {
		jQuery(this).parent().parent().find("textarea").val(jQuery(this).text());
		jQuery(this).parent().parent().find("textarea").addClass('is_replace');
		jQuery(this).parent().parent().find("textarea").trigger('keyup');
	}
});
jQuery("table td textarea").blur(function() {
	if( jQuery(this).val() == '' ) {
		jQuery(this).removeClass('is_replace');
	} else {
		jQuery(this).addClass('is_replace');
	}
});
jQuery(document).ready(function($)
{
	$("table td textarea").keyup(function()
	{
	var box=$(this).val();
	var rel=$(this).attr('rel');
	var main = box.length *100;
	var value= (main / 70);
	var count= box.length;
	 
	if(box.length <= 70)
	{
	$('#count' + rel).html(count);
	$('#bar' + rel).animate(
	{
	"width": value+'%',
	}, 1);
	}
	else
	{
		$('#count' + rel).html(count);
	}
	return false;
	});
	 
});
</script>
</div>