<?php

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>
<div class="wrap">

	<h2 class="nav-tab-wrapper">
		<?php foreach ($this->tabs as $tab): ?>
		<a class="nav-tab<?php if ($tab['is_active']) echo ' nav-tab-active'; ?>" href="<?php echo $tab['url']; ?>"><?php echo $tab['name']; ?></a>
		<?php endforeach; ?>
	</h2>
	
	<?php if (!empty($this->errors)): ?>
	<div class="error">
		<?php foreach ($this->errors as $err): ?>
		<p><strong><?php echo $err; ?></strong></p>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>

	<?php if (!empty($this->messages)): ?>
	<div class="updated">
		<?php foreach ($this->messages as $msg): ?>
		<p><strong><?php echo $msg; ?></strong></p>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
	
	<p><?php _e('Изменение настроек в момент выполнения экспорта любого вида ни на что не влияют. Новые настройки будут использованы  уже в следующий раз.', 'saphali-yandexmarket'); ?></p>
	
	<form method="post" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<h3><?php _e('Выбор категорий и товаров для прайса', 'saphali-yandexmarket'); ?></h3>
		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row"><label for="product_taxonomy"><?php _e('Таксономия товаров', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<select id="product_taxonomy" name="product_taxonomy">
							<?php
								if ($taxonomies) {
									foreach ($taxonomies as $taxonomy) {
										echo '<option value="' . $taxonomy->name . '"' . selected($taxonomy->name == $settings['product_taxonomy'], true, false) . '>' . esc_html($taxonomy->label) . '</option>';
									}
								}
							?>
						</select>
						<p class="description"><?php _e('После выбора таксономии и сохранения настроек нижние поля заполнятся соответствующими категориями.', 'saphali-yandexmarket'); ?></p>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="included_product_terms"><?php _e('Какие категории и связанные с ними товары нужно экспортировать?', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<select id="included_product_terms" name="included_product_terms[]" data-placeholder="Выбрать категории" multiple="multiple" size="8">
							<?php
								if ($all_product_terms) {
									echo '<option value="all" ' . selected(in_array('all', $settings['included_product_terms']), true, false) . '>' . __('Все перечисленные', 'saphali-yandexmarket') . '</option>';
									foreach ($all_product_terms as $cat) {
										echo '<option value="' . $cat->term_id . '"' . selected(in_array($cat->term_id, $settings['included_product_terms']), true, false) . '>' . $depth[$cat->term_id] . esc_html($cat->name) . '</option>';
									}
								} else {
									echo '<option value="">' . __('пусто', 'saphali-yandexmarket') . '</option>';
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="excluded_product_terms"><?php _e('Какие категории и связанные с ними товары исключить из экспорта?', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<select id="excluded_product_terms" name="excluded_product_terms[]" data-placeholder="Исключить категории" multiple="multiple" size="8">
							<?php
								if ($all_product_terms) {
									foreach ($all_product_terms as $cat) {
										echo '<option value="' . $cat->term_id . '"' . selected(in_array($cat->term_id, $settings['excluded_product_terms']), true, false) . '>' . $depth[$cat->term_id] . esc_html($cat->name) . '</option>';
									}
								} else {
									echo '<option value="">' . __('пусто', 'saphali-yandexmarket') . '</option>';
								}
							?>
						</select>
						<p class="description"><?php _e('Выбранные в данном списке категории и связанные с ними товары не будут экспортироваться даже если они выбранны в предыдущем списке. Товар, связанный одновременно с категориями из обоих списков, не попадёт в экспорт.', 'saphali-yandexmarket'); ?></p>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="excluded_product_terms_in_market"><?php _e('Сопоставление категорий магазина и категорий на Яндекс.Маркете', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<div class="product_terms_in_market">
						<table>
						<tr>
						<td><?php _e('Выбрать категории, которые необходимо сопоставить', 'saphali-yandexmarket'); ?>
						<select id="product_terms_in_market" name="product_terms_in_market[]" data-placeholder="Выбрать категории" multiple="multiple" size="8">
							<?php
								if ($all_product_terms) {
									foreach ($all_product_terms as $cat) {
										echo '<option value="' . $cat->term_id . '"' . selected(in_array($cat->term_id, $settings['product_terms_in_market']), true, false) . '>' . $depth[$cat->term_id] . esc_html($cat->name) . '</option>';
									}
								} else {
									echo '<option value="">' . __('пусто', 'saphali-yandexmarket') . '</option>';
								}
							?>
						</select></td>
						<td>
							<div class="product_terms_in_market_block"></div>
						</td>
						</tr>
						</table>
						<p class="description"><?php _e('Выбранные в данном списке категории и связанные с ними товары не будут экспортироваться даже если они выбранны в предыдущем списке. Товар, связанный одновременно с категориями из обоих списков, не попадёт в экспорт.', 'saphali-yandexmarket'); ?></p>
						</div>
					</td>
				</tr>
				<tr valign="top" class="single_select_page">
					<th scope="row" class="titledesc"><?php _e('Исключить товары', 'saphali-yandexmarket'); ?> </th>
					<td class="forminp">
					<input type="hidden" name="product_ex_shortcode_setting_save" value="1" />
					<select id="product_ex_select" name="product_ex_select[]" class="ajax_chosen_select_products" multiple="multiple" data-placeholder="<?php  echo 'Поиск товаров&hellip;'; ?>">
						<?php 
							$product_ids = !empty( $settings['product_ex_select'] ) ? array_map( 'trim', $settings['product_ex_select'] ) : false;
							if ($product_ids) {
								foreach ($product_ids as $product_id) {
									$title 	= get_the_title($product_id);
									$sku 	= get_post_meta($product_id, '_sku', true);

									if (!$title) continue;

									if (isset($sku) && $sku) $sku = ' ' . $sku . ' &ndash; '; else $sku = '#' . $product_id . ' &ndash; ';

									echo '<option value="'.$product_id.'" selected="selected">' . $sku . $title . '</option>';
								}
							}
						?>
					</select>
					</td>
				</tr>
				<tr>
					<th scope="row" rowspan="2"><?php _e('Товары под заказ', 'saphali-yandexmarket'); ?></th>
					<td style="margin-bottom: 0px; padding-bottom: 0px;">
						<label for="export_out_of_stock_products">
							<input id="export_out_of_stock_products" type="checkbox" name="export_out_of_stock_products" value="1" <?php checked($settings['export_out_of_stock_products'], false); ?> />
							<span><?php _e('Экспортировать товары, которых нет на складе, но доступны под заказ', 'saphali-yandexmarket'); ?></span>
						</label>
					</td><tr>
					<td class="export_out_of_stock_products_force"> 
						<label for="export_out_of_stock_products_force">
							<input id="export_out_of_stock_products_force" type="checkbox" name="export_out_of_stock_products_force" value="1" <?php checked($settings['export_out_of_stock_products_force'], true); ?> />
							<span><?php _e('Все товары, которых "нет в наличии" использовать как "товары под заказ".', 'saphali-yandexmarket'); ?></span>
						</label>
					</td></tr>
				</tr>
			</tbody>
		</table>
		
		
		<h3><?php _e('Настройка содержимого прайса', 'saphali-yandexmarket'); ?></h3>
		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row"><label for="nameshop"><?php _e('Короткое название магазина (name)', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<input id="nameshop" type="text" name="nameshop" value="<?php echo $settings['nameshop']; ?>" class="regular-text" />
						<small><?php _e('Не должно содержать более 20 символов', 'saphali-yandexmarket'); ?></small>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="company_name"><?php _e('Полное наименование компании, владеющей магазином (сompany)', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<input id="company_name" type="text" name="company_name" value="<?php echo $settings['company_name']; ?>" class="regular-text" />
						<small><?php _e('Не публикуется, используется для внутренней идентификации.', 'saphali-yandexmarket'); ?></small>
					</td>
				</tr>
				<tr valign="top" class="single_select_page">
					<th scope="row" class="titledesc"><label for="wikimart_is"><?php _e('Выгрузка в Wikimart', 'saphali-yandexmarket'); ?></label></th>
					<td class="forminp">
						<input type="checkbox" name="wikimart_is" value="1"<?php if( isset($settings['wikimart_is']) && $settings['wikimart_is'] ) { ?> checked="checked"<?php } ?> id="wikimart_is" />
					</td>
				</tr>
				<?php 
				$cur = apply_filters( 'woocommerce_currency', get_option('woocommerce_currency') );
				if( ($cur == "RUR" || $cur == "RUB" || $cur == "UAH") ) { $currency_h = ' hiddens'; } else { $currency_h = ''; } if ($currency_h != ' hiddens') { ?>
				<tr valign="top" class="single_select_page valute first">
					<th scope="row" class="titledesc"><?php _e('Курс валюты относительно рубля (пересчёт с валюты магазина)', 'saphali-yandexmarket'); ?></th>
					<td class="forminp">
						<input type="number" name="product_currency_ex" value="<?php if(isset($settings['product_currency_ex'])) echo $settings['product_currency_ex']; ?>" placeholder="0.000" /> <small><?php _e(sprintf('Например, если основная валюта USD то значение должно быть %f (для рубля). Если оставить пустым, то будет использоваться валюта по умолчанию.', 65), 'saphali-yandexmarket'); ?></small>
					</td>
				</tr>
				<?php } ?>
				<tr valign="top" class="single_select_page<?php echo $currency_h;?> valute">
					<th scope="row" class="titledesc"><?php _e('Курс валюты относительно рубля для Я.М', 'saphali-yandexmarket'); ?></th>
					<td class="forminp">
						<?php _e('Доллар', 'saphali-yandexmarket'); ?> <input type="number" name="product_currency_USD" value="<?php if(isset($settings['product_currency_USD'])) echo $settings['product_currency_USD']; else echo $this->get_currency_rate_shipping( 'USD', "RUB"); ?>" placeholder="0.000" /> <br />
						<?php _e('Евро', 'saphali-yandexmarket'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="number" name="product_currency_EUR" value="<?php if(isset($settings['product_currency_EUR'])) echo $settings['product_currency_EUR']; else echo $this->get_currency_rate_shipping('EUR', "RUB"); ?>" placeholder="0.000" /> <br />
						<span id="grivna"><?php _e('Гривна', 'saphali-yandexmarket'); ?></span> <input type="number" name="product_currency_UAH" value="<?php if(isset($settings['product_currency_UAH'])) echo $settings['product_currency_UAH']; else echo $this->get_currency_rate_shipping('UAH', "RUB"); ?>" placeholder="0.000" /> 
						<br /><small><?php _e('Данный курс будет использоваться в Яндекс.Маркете. Если оставить незаполненным, то будет значение по умолчанию CBRF (ЦБРФ - центральный банк РФ)', 'saphali-yandexmarket'); ?>.</small>
					</td>
				</tr>
				<?php if(!$currency_h) { ?>
				<tr valign="top" class="single_select_page">
					<th scope="row" class="titledesc"><label for="curencyInShop"><?php _e('Выгружать цены и валюту в валюте магазина', 'saphali-yandexmarket'); ?> (<?php  echo $cur; ?>)</label></th>
					<td class="forminp">
						<input type="checkbox" name="curencyInShop" value="1"<?php if( isset($settings['curencyInShop']) && $settings['curencyInShop'] ) { ?> checked="checked"<?php } ?> id="curencyInShop" />
					</td>
				</tr>
				<?php } ?>
				
				
				<tr>
					<th scope="row"><label for="product_local_delivery_cost_"><?php _e('Условия доставки для всех товаров', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<?php $_settings = $settings['product_local_delivery_cost_all']; for($i=0; $i < sizeof( $_settings['product_local_delivery_cost']) || sizeof( $_settings['product_local_delivery_cost']) == 0 && $i === 0; $i++ ) { 
						$_settings['product_local_delivery_time'][$i] = isset($_settings['product_local_delivery_time'][$i]) ? $_settings['product_local_delivery_time'][$i] : '';
						$_settings['product_local_delivery_days'][$i] = isset($_settings['product_local_delivery_days'][$i]) ? $_settings['product_local_delivery_days'][$i] : '';
						$_settings['product_local_delivery_cost'][$i] = isset($_settings['product_local_delivery_cost'][$i]) ? $_settings['product_local_delivery_cost'][$i] : '';
						?>
						<div class="delivery-options">
						<span class="else">Условие №<?php echo $i+1; ?></span>
						Стоимость <br />
						<input id="product_local_delivery_cost<?php echo $i; ?>" type="number" name="product_local_delivery_cost[]" min='0' step="10" value="<?php echo $_settings['product_local_delivery_cost'][$i]; ?>" class="regular-text" />
						<hr />
						Срок <br />
						<input id="product_local_delivery_days<?php echo $i; ?>" type="text" name="product_local_delivery_days[]" value="<?php echo $_settings['product_local_delivery_days'][$i]; ?>"  />
						<?php if($i === 0) { ?> 
						<br />
						<span class="description">Если срок доставки сильно варьируется, например составляет от двух до семи дней, в данном поле необходимо указать период с максимальным сроком, при этом весь период должен составлять не более трех дней по правилам передачи этой опции (допустимые периоды: 1-3, 2-4, 3-5, 4-6 и т.д.). Таким образом, для срока доставки 2‒7 дней необходимо ввести <strong>"5-7"</strong> или для конкретного срока доставки, если магазин доставляет все товары на следующий день то необходимо указать "<strong>1</strong>".  </span>
						<?php } ?>
						<hr />
						Время <br />
						<input id="product_local_delivery_time<?php echo $i; ?>" type="number" name="product_local_delivery_time[]" min='0' max='24' step="1" value="<?php echo $_settings['product_local_delivery_time'][$i]; ?>" class="regular-text" />
						<?php if($i === 0) { ?> 
						<br />
						<span class="description">Если срок доставки зависит от времени оформления заказа , то укажите время в часовом поясе магазина (в часах, от 0 до 24), например, если указать "<strong>14</strong>", то если заказ оформлен до 14:00 текущего дня, то магазин доставляет все товары согласно сроку доставки указанного в поле "Срок", а если заказ оформлен после 14:00, то товары будут доставлены на день позже указанного значения. Указание атрибута необязательно, по умолчанию используется значение 24 (полночь).</span>
						<?php } ?>
						</div>
						<?php } ?>
						<span class="button add_delivery_option">Добавить еще</span>
					</td>
				</tr>
				<!--<tr>
					<th scope="row"><label for="product_local_delivery_cost"><?php _e('Общая стоимость доставки для всех товаров', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<input id="product_local_delivery_cost" type="number" name="product_local_delivery_cost" min='0' step="0.1" value="<?php echo $settings['product_local_delivery_cost']; ?>" class="regular-text" />
					</td>
				</tr> -->
				<tr>
					<th scope="row"><label for="is_export_store_delivery"><?php _e('Выгрузить информацию о возможности доставки, самовывоза или покупки товара в магазине-салоне (&#x3C;delivery&#x3E;, &#x3C;pickup&#x3E; и &#x3C;store&#x3E;)', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<input type="checkbox" id="is_export_store_delivery" value="1" <?php if( isset($settings['is_export_store_delivery']) && $settings['is_export_store_delivery'] ) { ?> checked="checked"<?php } ?> name="is_export_store_delivery">
					</td>
				</tr>
				
				<tr valign="top" class="single_select_page hiddens_delivery">
					<th scope="row" class="titledesc"><label for="is_export_delivery"><?php _e('Есть возможность доставки', 'saphali-yandexmarket'); ?> (&#x3C;delivery&#x3E;)</label></th>
					<td class="forminp">
						<input type="checkbox" name="is_export_delivery" value="1"<?php if( isset($settings['is_export_delivery']) && $settings['is_export_delivery'] ) { ?> checked="checked"<?php } ?> id="is_export_delivery" /> <br />
						
						<input type="checkbox" name="is_export_delivery_echo" value="1"<?php if( isset( $settings['is_export_delivery_echo'] ) && $settings['is_export_delivery_echo'] ) { ?> checked="checked"<?php } ?> id="is_export_delivery_echo" /> <label for="is_export_delivery_echo"><?php _e('Не выгружать этот параметр', 'saphali-yandexmarket'); ?></label>
					</td>
				</tr>
				
				<tr valign="top" class="single_select_page hiddens_delivery">
					<th scope="row" class="titledesc"><label for="is_export_pickup"><?php _e('Есть возможность самовывоза', 'saphali-yandexmarket'); ?> (&#x3C;pickup&#x3E;)</label></th>
					<td class="forminp">
						<input type="checkbox" name="is_export_pickup" value="1"<?php if( isset($settings['is_export_pickup']) && $settings['is_export_pickup'] ) { ?> checked="checked"<?php } ?> id="is_export_pickup" /> <br />
						
						<input type="checkbox" name="is_export_pickup_echo" value="1"<?php if( isset($settings['is_export_pickup_echo']) && $settings['is_export_pickup_echo'] ) { ?> checked="checked"<?php } ?> id="is_export_pickup_echo" /> <label for="is_export_pickup_echo"><?php _e('Не выгружать этот параметр', 'saphali-yandexmarket'); ?></label>
					</td>
				</tr>
				
				<tr valign="top" class="single_select_page hiddens_delivery">
					<th scope="row" class="titledesc"><label for="is_export_store"><?php _e('Есть возможность покупки товара в магазине-салоне', 'saphali-yandexmarket'); ?> (&#x3C;store&#x3E;)</label></th>
					<td class="forminp">
						<input type="checkbox" name="is_export_store" value="1"<?php if( isset($settings['is_export_store']) && $settings['is_export_store'] ) { ?> checked="checked"<?php } ?> id="is_export_store" /> <br />
						
						<input type="checkbox" name="is_export_store_echo" value="1"<?php if( isset($settings['is_export_store_echo']) && $settings['is_export_store_echo'] ) { ?> checked="checked"<?php } ?> id="is_export_store_echo" /> <label for="is_export_store_echo"><?php _e('Не выгружать этот параметр', 'saphali-yandexmarket'); ?></label>
					</td>
				</tr>
				
				<tr>
					<th scope="row"><?php _e('Описание товара', 'saphali-yandexmarket'); ?></th>
					<td>
						<label>
							<input type="radio" name="description_source" value="" <?php checked($settings['description_source'], ''); ?> />
							<span><?php _e('Не добавлять в прайс', 'saphali-yandexmarket'); ?></span>
						</label>
						<br />
						<label>
							<input type="radio" name="description_source" value="post_excerpt" <?php checked($settings['description_source'], 'post_excerpt'); ?> />
							<span><?php _e('Краткое описание в качестве источника', 'saphali-yandexmarket'); ?></span>
						</label>
						<br />
						<label>
							<input type="radio" name="description_source" value="post_content" <?php checked($settings['description_source'], 'post_content'); ?> />
							<span><?php _e('Полное описание в качестве источника', 'saphali-yandexmarket'); ?></span>
						</label>
					</td>
				</tr>
				<tr valign="top" class="single_select_page">
					<th scope="row" class="titledesc"><label for="product__no_sku"><?php _e('Включить vendorCode (Артикул)', 'saphali-yandexmarket'); ?></label></th>
					<td class="forminp">
						<input type="checkbox" name="product__no_sku" id="product__no_sku" value="1"<?php if( isset($settings['product__no_sku']) && $settings['product__no_sku'] ) { ?> checked="checked"<?php } elseif(!isset($settings['product__no_sku'])) { ?> checked="checked"<?php } ?> />
					</td>
				</tr>
				<tr valign="top" class="single_select_page">
					<th scope="row" class="titledesc"><label for="product__variable"><?php _e('Экспортировать каждый вариант в отдельности (вариативные товары)', 'saphali-yandexmarket'); ?></label></th>
					<td class="forminp">
						<input type="checkbox" name="product__variable" id="product__variable" <?php if( isset($settings['product__variable']) && $settings['product__variable'] ) { ?> checked="checked"<?php } elseif(!isset($settings['product__variable'])) { ?> checked="checked"<?php } ?> value="1" />
					</td>
				</tr>
				<tr valign="top" class="single_select_page bid">
					<th scope="row" class="titledesc"><?php _e('Основная ставка (bid)', 'saphali-yandexmarket'); ?></th>
					<td class="forminp">
						<input type="text" name="bid" value="<?php if( isset($settings['bid']) ) echo $settings['bid']; ?>" placeholder="не обязательно" />
					</td>
				</tr>
				<tr valign="top" class="single_select_page cbid">
					<th scope="row" class="titledesc"><?php _e('Ставка на клик для карточек (cbid)', 'saphali-yandexmarket'); ?></th>
					<td class="forminp">
						<input type="text" name="cbid" value="<?php if( isset($settings['cbid']) ) echo $settings['cbid']; ?>" placeholder="не обязательно" /> <small><?php _e('Действует только на карточках моделей.', 'saphali-yandexmarket'); ?></small>
					</td>
				</tr>
				
				
				<tr valign="top" class="single_select_page IS_VENDOR">
					<th scope="row" class="titledesc"><label for="product_vendor_model"><?php _e('Использовать формат: произвольный товар (vendor.model)', 'saphali-yandexmarket'); ?>?</label></th>
					<td class="forminp">
						<input type="checkbox" name="product_vendor_model" id="product_vendor_model" value="1"<?php if( isset($settings['product_vendor_model']) && $settings['product_vendor_model'] ) { ?> checked="checked"<?php } ?> /> <?php _e('Yes'); ?> <br />
						<span class="descripttion"><em><?php _e('Для указания производителя и модели, в отличии от упрощенного описания', 'saphali-yandexmarket'); ?></em></span>
					</td>
				</tr>
				<tr valign="top" class="single_select_page model">
					<th scope="row" class="titledesc"><?php _e('Определить таксономию для модели товара', 'saphali-yandexmarket'); ?></th>
					<td class="forminp">
						<select id="product_model_edit" name="product_model_edit">
							<?php
								if ($taxonomies) {
									echo '<option value="" ' . selected($settings['product_model_edit'] == '', true, false) . '>Нет</option>';
									foreach ($taxonomies as $taxonomy) {
										echo '<option value="' . $taxonomy->name . '"' . selected($taxonomy->name == $settings['product_model_edit'], true, false) . '>' . esc_html($taxonomy->label) . '</option>';
									}
								}
							?>
						</select>
					</td>
				</tr>

				<tr>
					<th scope="row"><label for="vendor_taxonomy"><?php _e('Производитель', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<select id="vendor_taxonomy" name="product_vendor_edit">
							<?php
								if ($taxonomies) {
									echo '<option value="" ' . selected($settings['product_vendor_edit'] == '', true, false) . '>Нет</option>';
									foreach ($taxonomies as $taxonomy) {
										echo '<option value="' . $taxonomy->name . '"' . selected($taxonomy->name == $settings['product_vendor_edit'], true, false) . '>' . esc_html($taxonomy->label) . '</option>';
									}
								}
							?>
						</select>
						<p class="description"><?php _e('Выберите таксономию производителей или оставьте поле пустым, чтобы не включать производителя в прайс.', 'saphali-yandexmarket'); ?></p>
					</td>
				</tr>
				<tr valign="top" class="single_select_page no_typePrefix model">
					<th scope="row" class="titledesc"><label for="product_no_typePrefix"><?php _e('Не выгружать тег <strong>typePrefix</strong> при формате товара vendor.model', 'saphali-yandexmarket'); ?></label></th>
					<td class="forminp">
						<input type="checkbox" name="product_no_typePrefix" id="product_no_typePrefix" value="1"<?php if( isset($settings['product_no_typePrefix']) && $settings['product_no_typePrefix'] ) { ?> checked="checked"<?php } ?> /> <?php _e('Yes'); ?> <br />
						<span class="descripttion"><em><?php _e('Если выгружается этот тэг, то в названии товара фигурирует в начале его категория', 'saphali-yandexmarket'); ?></em></span>
					</td>
				</tr>
				<tr valign="top" class="single_select_page">
					<th scope="row" class="titledesc"><?php _e('Определить таксономию для страны производства товара', 'saphali-yandexmarket'); ?> (<strong>country_of_origin</strong>)</th>
					<td class="forminp">
					<select id="country_of_origin_taxonomy" name="product_country_of_origin">
					<?php if ($taxonomies) {
							echo '<option value="" ' . selected($settings['product_country_of_origin'] == '', true, false) . '>Нет</option>';
							foreach ($taxonomies as $taxonomy) {
								echo '<option value="' . $taxonomy->name . '"' . selected($taxonomy->name == $settings['product_country_of_origin'], true, false) . '>' . esc_html($taxonomy->label) . '</option>';
							}
						} ?>
					</select>
					</td>
				</tr>

				<tr>
					<th scope="row"><?php _e('Фотография', 'saphali-yandexmarket'); ?></th>
					<td>
						<label for="add_image">
							<input  id="add_image" type="checkbox" name="add_image" value="1" <?php checked($settings['add_image'], 1); ?> />
							<span><?php _e('Добавить в прайс', 'saphali-yandexmarket'); ?></span>
						</label>
					</td>
				</tr>
				<tr valign="top" class="sales_notes">
					<th scope="row" class="titledesc"><?php _e( sprintf( 'Примечание (%s) к товарам (будет добавлено ко всем товарам, кроме товаров доступных под заказ, если поле ниже будет заполнено)', '<strong>sales_notes</strong>'), 'saphali-yandexmarket'); ?>. </th>
					<td class="forminp">
						<textarea name="sales_notes_yml"><?php if( isset($settings['sales_notes_yml']) ) {
							echo $settings['sales_notes_yml'];
						} ?></textarea>
					</td>
				</tr>
				<tr valign="top" class="sales_notes_no_valiable">
					<th scope="row" class="titledesc"><?php _e( sprintf( 'Примечание (%s) к товарам (будет добавлено к товарам доступных под заказ)', '<strong>sales_notes</strong>'), 'saphali-yandexmarket'); ?>. </th>
					<td class="forminp">
						<textarea name="sales_notes_yml_no_valiable"><?php if( isset($settings['sales_notes_yml_no_valiable']) ) {
							echo $settings['sales_notes_yml_no_valiable'];
						} ?></textarea>
					</td>
				</tr>
				<tr valign="top" class="fix_title">
					<th scope="row" class="titlefix"><?php _e( 'URL старого сайта', 'saphali-yandexmarket'); ?> </th>
					<td class="forminp">
						<input type="text" name="fix_title" value="<?php if( isset($settings['fix_title']) ) echo $settings['fix_title']; ?>" placeholder="не обязательно" /> <br />
						<small><?php echo sprintf( __('Если сайт раньше имел другой домен, и товары были заполнены на старом домене (т.е. не на %s), то укажите URL старого сайта здесь. [Это решит проблему с URL вида: 1) http://<strong><em %s>domen.com</em></strong>/http&#x25;3A//<strong><em %s>shop.domen.com</em></strong>/wp-content/uploads/2013/08/520a42a45ebaa.jpg или 2) http://<strong><em %s>shop.domen.com</em></strong>/http&#x25;3A//<strong><em %s>domen.com</em></strong>/wp-content/uploads/2013/08/520a42a45ebaa.jpg, и чтобы решить проблему c URL в данном примере необходимо указать старый URL, домен которого можно наблюдать после текста <strong>"http&#x25;3A//"</strong> или <strong>"https&#x25;3A//"</strong> - <em>http://shop.domen.com</em> (на примере первой ссылки). Если данной проблемы нет, то проигнорируйте данное поле].', 'saphali-yandexmarket'), home_url(), 'style="font-size: 120%;"', 'style="font-size: 120%;"', 'style="font-size: 120%;"', 'style="font-size: 120%;"', 'style="font-size: 120%;"' ); ?></small>
					</td>
				</tr>
			</tbody>
		</table>
		<script type='text/javascript' src='<?php echo SYMR_URL . 'chosen/chosen.jquery.js'; ?>'></script>
		<link rel='stylesheet' href='<?php echo SYMR_URL . 'chosen/chosen.css'; ?>' type='text/css' media='all' />

		<style type="text/css">
		#product_ex_select, #excluded_product_terms, #_product_terms_in_market, #product_terms_in_market, #included_product_terms {width: 350px;}
		#product_ex_select_chosen input, #excluded_product_terms_chzn input, #included_product_terms_chzn input { height: auto; padding: 2px; }
		td.export_out_of_stock_products_force {width: auto!important;}
		div.delivery-options.even{background: #e1e1e1 none repeat scroll 0 0;}
		.delivery-options span.else {display: block;font-weight: bold;}
		div.delivery-options {border: 1px solid #bcbcbc;margin-bottom: 3px;padding: 10px;}
		.product_terms_in_market td { vertical-align: top; width: 50%; }
		.product_terms_in_market_block div > label {display: block;font-style: italic;font-weight: bold;}
		.product_terms_in_market_block div > label::after {content: " сопоставить с ";font-style: normal;font-weight: normal;}
		.in_market_bloc {margin-bottom: 7px;}
		</style>
		<script type="text/javascript">
		jQuery("select#included_product_terms, select#excluded_product_terms, select._product_terms_in_market, select#product_terms_in_market, #product_model_edit, #vendor_taxonomy, #country_of_origin_taxonomy, #product_taxonomy").chosen();	
		function nth_child_delivery_option () {
			jQuery("div.delivery-options:nth-child(2n+2)").addClass('even');
			if( jQuery("div.delivery-options").length > 4 ) {
				jQuery("span.add_delivery_option").hide();
			}
		}
		jQuery(function($){
			$('body').on('click', ".product_terms_in_market_block_all", function(event){
				event.preventDefault();
				var _this = $(this).parent();
				_this.css({'opacity': '.6', 'background': '#fff none repeat scroll 0 0', 'cursor': 'wait' });
				var e = _this.attr("class").replace(/in_market_bloc |product_terms_in_market_block/g, '') 
				var name = $('select#product_terms_in_market option[value="' + e + '"').text();
				var _data = {action: "saphali_select_ym_category", name: name, cat_id: e, all: 1 };
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: '<?php echo site_url('wp-admin'); ?>/admin-ajax.php?debug',
					data: _data,
					success: function(response)
					{
						var option = "<option value=''>Выбрать...</option>";
						$.each(response, function(i_name,e_name){
							if(e_name.select == true)
							option = option + "<option selected='selected' value='" + e_name.name + "'>" + e_name.name + "</option>";
							else
							option = option + "<option value='" + e_name.name + "'>" + e_name.name + "</option>";
						});
						$(".product_terms_in_market_block" + e ).html( '<label>' + name + '</label>' + '<button class="product_terms_in_market_block_all">Подгрузить все товары</button>' + '<select class="_product_terms_in_market" style="width:320px" id="_product_terms_in_market' + e + '" name="_product_terms_in_market[' + e + ']" data-placeholder="Выберите категорию">\
					' + option + '</select>');
						
						$('#_product_terms_in_market' + e ).chosen({
							disable_search_threshold: 10,
							include_group_label_in_selected: true,
							no_results_text: "<?php _e("Не найдено!", 'saphali-yandexmarket'); ?>",
							search_contains: true,
							width: '320px'
						 });
						 _this.css({'opacity': '1', 'background': 'transparent none repeat scroll 0 0', 'cursor': 'normal' });
					}
				});
			});
			$('body').on('change', "select#product_terms_in_market", function(){
				// console.log($(this).val());
				var value = $(this).val();
				$.each( $(".product_terms_in_market_block div.in_market_bloc" ), function(i,e) {
					var id = $(this).attr("class").replace(/in_market_bloc |product_terms_in_market_block/g, '');
					// console.log(id);
					if( $.inArray(id, value) < 0 )
						 $(this).remove();
				});
				
				if(value)
				$.each( value, function(i,e) {
					$(".product_terms_in_market_block" + e ).remove();
					$(".product_terms_in_market_block" ).html( $(".product_terms_in_market_block").html() + '<div class="in_market_bloc product_terms_in_market_block' + e + '"></div>');
					var name = $('select#product_terms_in_market option[value="' + e + '"').text();
					var _data = {action: "saphali_select_ym_category", name: name, cat_id: e };
					$.ajax({
						type: 'POST',
						dataType: 'json',
						url: '<?php echo site_url('wp-admin'); ?>/admin-ajax.php?debug',
						data: _data,
						success: function(response)
						{
							var option = "<option value=''>Выбрать...</option>";
							$.each(response, function(i_name,e_name){
								if(e_name.select == true)
								option = option + "<option selected='selected' value='" + e_name.name + "'>" + e_name.name + "</option>";
								else
								option = option + "<option value='" + e_name.name + "'>" + e_name.name + "</option>";
							});
							$(".product_terms_in_market_block" + e ).html( '<label>' + name + '</label>' + '<button class="product_terms_in_market_block_all">Подгрузить все категории</button>' + '<select class="_product_terms_in_market" style="width:320px" id="_product_terms_in_market' + e + '" name="_product_terms_in_market[' + e + ']" data-placeholder="Выберите категорию">\
						' + option + '</select>');
							
							$('#_product_terms_in_market' + e ).chosen({
								disable_search_threshold: 10,
								include_group_label_in_selected: true,
								no_results_text: "<?php _e("Не найдено!", 'saphali-yandexmarket'); ?>",
								search_contains: true,
								width: '320px'
							 });
						}
					});
				});
			});
			$("select#product_terms_in_market").trigger("change");
			
			$('body').on('click', "span.add_delivery_option", function(){
				if( $("div.delivery-options").length < 5 ) {
					$("div.delivery-options:last").after('<div class="delivery-options"><span class="else">Условие №' + ( $("div.delivery-options").length + 1) + '</span>Стоимость <br /><input id="product_local_delivery_cost' + $("div.delivery-options").length + '" type="number" name="product_local_delivery_cost[]" min="0" step="10" value="" class="regular-text" /><hr />Срок <br /><input id="product_local_delivery_days' + $("div.delivery-options").length + '" type="text" name="product_local_delivery_days[]" value="" /><hr />Время <br /><input id="product_local_delivery_time' + $("div.delivery-options").length + '" type="number" name="product_local_delivery_time[]" min="0" max="24" step="1" value="" class="regular-text" /></div>');
					nth_child_delivery_option ();
				}
				
			});
			nth_child_delivery_option ();
			if( !$("input[name='product_vendor_model']").is(":checked") ) {
				$("tr.model").hide();
			}
			$("table.form-table tr.IS_VENDOR").delegate("input[name='product_vendor_model']", 'click',function() {
				if($(this).is(":checked")) {
					$("tr.model").show('slow');
				} else {
					$("tr.model").hide('slow');
				}
			});
			if( $("#curencyInShop").is(":checked") ) {
				$("tr.valute.single_select_page").hide('slow');
			} else { $("tr.valute.single_select_page").show('slow');  }
			
			$("table.form-table").delegate('#export_out_of_stock_products', 'click',function() {
				if( $(this).is(":checked") ) {
					$("td.export_out_of_stock_products_force").show('slow');
				} else {
					$("td.export_out_of_stock_products_force").hide('slow');
				}
			});
			$("tr.single_select_page").delegate('#curencyInShop', 'click',function() {
				if( $(this).is(":checked") ) {
					$("tr.valute.single_select_page").hide('slow');
				} else  {
					$("tr.valute.single_select_page").show('slow'); 
					if( $("tr.single_select_page #wikimart_is").is(":checked") ) {
						$("tr.country.single_select_page").hide('slow');
					} else  { $("tr.hiddens.single_select_page").hide('slow'); }
				}
			});
			
			if( $("#wikimart_is").is(":checked") ) {
				$("tr.hiddens.single_select_page th.titledesc").text('<?php _e('Курс валюты относительно', 'saphali-yandexmarket'); ?> <?php if( apply_filters( 'woocommerce_currency', get_option('woocommerce_currency') ) == "UAH" ) _e('гривны', 'saphali-yandexmarket'); else _e('рубля', 'saphali-yandexmarket'); ?> <?php _e('для Я.М', 'saphali-yandexmarket'); ?>');
				$("tr.hiddens.single_select_page #grivna").text('<?php if( apply_filters( 'woocommerce_currency', get_option('woocommerce_currency') ) == "UAH" ) echo 'Рубль'; else _e('Гривна', 'saphali-yandexmarket'); ?>');
				$("tr.country.single_select_page").hide('slow');
			} else { $("tr.hiddens.single_select_page").hide();  }
			$("tr.single_select_page").delegate('#wikimart_is', 'click',function() {
				if( $(this).is(":checked") ) { $("tr.hiddens.single_select_page").show('slow'); $("tr.hiddens.single_select_page th.titledesc").text('<?php _e('Курс валюты относительно', 'saphali-yandexmarket'); ?> <?php if( apply_filters( 'woocommerce_currency', get_option('woocommerce_currency') ) == "UAH" ) _e('гривны', 'saphali-yandexmarket'); else _e('рубля', 'saphali-yandexmarket'); ?> <?php _e('для Я.М', 'saphali-yandexmarket'); ?>'); $("tr.country.single_select_page").hide('slow');
				$("tr.hiddens.single_select_page #grivna").text('<?php if( apply_filters( 'woocommerce_currency', get_option('woocommerce_currency') ) == "UAH" ) echo 'Рубль'; else  _e('Гривна', 'saphali-yandexmarket'); ?>');
				} else  { $("tr.hiddens.single_select_page").hide('slow'); $("tr.hiddens.single_select_page th.titledesc").text('<?php _e('Курс валюты относительно', 'saphali-yandexmarket'); ?> <?php _e('рубля', 'saphali-yandexmarket'); ?> <?php _e('для Я.М', 'saphali-yandexmarket'); ?>'); $("tr.hiddens.single_select_page #grivna").text('<?php _e('Гривна', 'saphali-yandexmarket'); ?>');
				$("tr.country.single_select_page").show('slow');
				
				}
			});
			
			if( $("#is_export_store_delivery").is(":checked") ) {
				//$("tr.country.single_select_page").hide('slow');
			} else { $("tr.hiddens_delivery.single_select_page").hide();  }
			$("tr").delegate('#is_export_store_delivery', 'click',function() {
				if( $(this).is(":checked") ) {
					$("tr.hiddens_delivery.single_select_page").show('slow');
				} else  { $("tr.hiddens_delivery.single_select_page").hide('slow'); }
			});
			$("tr").delegate('#is_export_delivery, #is_export_pickup, #is_export_store', 'click',function() {
				if( $(this).is(":checked") ) {
					$(this).parent().children("#"+$(this).attr("id")+"_echo").attr('checked',false);
					$(this).parent().children("#"+$(this).attr("id")+"_echo, label[for='"+$(this).attr("id")+"_echo']").hide('slow');
				}
				else {
					$(this).parent().children("#"+$(this).attr("id")+"_echo, label[for='"+$(this).attr("id")+"_echo']").show('slow');
				}
					
			});
			if( ! $("#export_out_of_stock_products").is(":checked") ) {
				$("td.export_out_of_stock_products_force").hide();
			}
			if($("#is_export_delivery").is(":checked") ) {
				$("#is_export_delivery").parent().children("#"+$("#is_export_delivery").attr("id")+"_echo").attr('checked',false);
				$("#is_export_delivery").parent().children("#"+$("#is_export_delivery").attr("id")+"_echo, label[for='"+$("#is_export_delivery").attr("id")+"_echo']").hide();
			}
					
			if($("#is_export_pickup").is(":checked") ) {
				$("#is_export_pickup").parent().children("#"+$("#is_export_pickup").attr("id")+"_echo").attr('checked',false);
				$("#is_export_pickup").parent().children("#"+$("#is_export_pickup").attr("id")+"_echo, label[for='"+$("#is_export_pickup").attr("id")+"_echo']").hide();
			}
					
			if($("#is_export_store").is(":checked") ) {
				$("#is_export_store").parent().children("#"+$("#is_export_store").attr("id")+"_echo").attr('checked',false);
				$("#is_export_store").parent().children("#"+$("#is_export_store").attr("id")+"_echo, label[for='"+$("#is_export_store").attr("id")+"_echo']").hide();
			}
		});
		</script>
		<?php submit_button(); ?>
		
	</form>
</div>