<?php

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>
<div class="wrap">

	<h2 class="nav-tab-wrapper">
		<?php foreach ($this->tabs as $tab): ?>
		<a class="nav-tab<?php if ($tab['is_active']) echo ' nav-tab-active'; ?>" href="<?php echo $tab['url']; ?>"><?php echo $tab['name']; ?></a>
		<?php endforeach; ?>
	</h2>
	
	<?php if (!empty($this->errors)): ?>
	<div class="error">
		<?php foreach ($this->errors as $err): ?>
		<p><strong><?php echo $err; ?></strong></p>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
	
	<?php if (!empty($this->messages)): ?>
	<div class="updated">
		<?php foreach ($this->messages as $msg): ?>
		<p><strong><?php echo $msg; ?></strong></p>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
	<p>
		<?php _e('Включенный экспорт запускается каждый день. Конкретное время запуска посреди дня настраивается тремя способами:', 'saphali-yandexmarket'); ?><br />
		<?php _e(' - только в 9:15, например (периодичность нужно указать: раз в сутки);', 'saphali-yandexmarket'); ?><br />
		<?php _e(' - в 9:15 и 21:15, например (в поле "Время запуска", при этом, можно ввести как 9:15, так и 21:15, а периодичность нужно указать: дважды в сутки);', 'saphali-yandexmarket'); ?><br />
		<?php _e(' - каждый час (в поле "Время запуска", при этом, можно ввести любое время, а периодичность нужно указать: каждый час).', 'saphali-yandexmarket'); ?>
	</p>
	<p><?php _e('Если Ваш сайт постоянно посещает по несколько пользователей за час, то экспорт будет запускаться более-менее по расписанию и можно больше ничего не настраивать (Wordpress будет использовать свой виртуальный крон). Но в противном случае настоятельно рекомендуем отключить крон Wordpress и настроить крон в Вашем хостинг-аккаунте, указав такие же время запуска и периодичность. Крон на хостинге всегда запускается автоматически и вовремя.', 'saphali-yandexmarket'); ?></p>
	
	<form method="post" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>">	
		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row"><?php _e('Активность', 'saphali-yandexmarket'); ?></th>
					<td>
						<label for="auto_export_status">
							<input id="auto_export_status" type="checkbox" name="auto_export_status" value="1" <?php checked($settings['auto_export_status'], 1); ?> />
							<span><?php _e('Включить', 'saphali-yandexmarket'); ?></span>
						</label>
					</td>
				</tr>
				<tr>
					<th scope="row" ><label for="auto_export_first_time"><?php _e('Время запуска', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<input id="auto_export_first_time" type="text" name="auto_export_first_time" value="<?php echo $settings['auto_export_first_time']; ?>" class="small-text" maxlength="5" />
						<p class="description"><?php _e('В формате ЧЧ:ММ согласно часовому поясу, выбранному в настройках Wordpress.', 'saphali-yandexmarket'); ?></p>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="auto_export_recurrence"><?php _e('Периодичность', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<select id="auto_export_recurrence" name="auto_export_recurrence">
							<option value="daily" <?php selected($settings['auto_export_recurrence'] == 'daily'); ?>><?php _e('раз в сутки', 'saphali-yandexmarket'); ?></option>
							<option value="twicedaily" <?php selected($settings['auto_export_recurrence'] == 'twicedaily'); ?>><?php _e('дважды в сутки', 'saphali-yandexmarket'); ?></option>
							<option value="hourly" <?php selected($settings['auto_export_recurrence'] == 'hourly'); ?>><?php _e('каждый час', 'saphali-yandexmarket'); ?></option>
						</select>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="auto_export_errors_mail"><?php _e('E-mail для сообщений об ошибках', 'saphali-yandexmarket'); ?></label></th>
					<td>
						<input id="auto_export_errors_mail" type="text" name="auto_export_errors_mail" value="<?php echo $settings['auto_export_errors_mail']; ?>" class="regular-text" />
					</td>
				</tr>
			</tbody>
		</table>
		<?php submit_button(); ?>
	</form>
</div>